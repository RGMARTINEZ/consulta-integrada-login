
drop table usuarios;
drop table log_inserts;



create table usuarios(
id   			int identity(1,1)not null,
tipo		    varchar(20)null,
username		varchar(20)null,
password		varchar(10)null,
nombres			varchar(60)null,
apellidos		varchar(60)null,
borrado			varchar(1) not null,
activo			varchar(1) not null,  
fecha_creacion	DATE NOT NULL,
primary key( id )
);

create table log_inserts(
id   			int identity(1,1)not null,
id_tipo_documento      varchar(50) not null,
numero_documento           varchar(20) not null,
primer_nombre              varchar(100)null,
segundo_nombre		       varchar(100)null,
primer_apellido  	       varchar(100)null,
segundo_apellido            varchar(100)null,
borrado			varchar(1) not null,
activo			varchar(1) not null, 
usuario_creacion        varchar(50) not null,  
fecha_creacion	DATE NOT NULL,
primary key( id )
);



  /* USUARIOS SDP */

insert into usuarios values ('Usuario','nvasquez','ong4fwiJfO','Ninfa Luz','Vasquez','N','A',GETDATE());

insert into usuarios values ('Usuario','jhernandez','XSMZegPcKl','Jose Luis','Hernandez','N','A',GETDATE());

insert into usuarios values ('Usuario','hmenjura','8VMqyiVap1','Helmut','Menjura','N','A',GETDATE());

insert into usuarios values ('Usuario','rguzman','1983guzman','Rafael de Jesus','Guzman Martinez','N','A',GETDATE());

insert into usuarios values ('Usuario','ptorresm','ZOA1u7a9YQ','Paola Yanira','Sierra Montaña','N','A',GETDATE());

insert into usuarios values ('Usuario','csierra','Rk8r1pccOo','Cesar','Sierra Medina','N','A',GETDATE());

insert into usuarios values ('Usuario','yrodriguezg','7sG91nOgpM','Yenny','Rodriguez Gonzalez','N','A',GETDATE());

insert into usuarios values ('Usuario','lgarzon','gvHAlpUe6r','Luz Yanira','Garzon Ardila','N','A',GETDATE());

insert into usuarios values ('Usuario','aayala','bT953XaOPP','Alexi','Ayala Chacon','N','A',GETDATE());

insert into usuarios values ('Usuario','ymanrique','XKOhmOtHPD','Yivi','Manrique Vacca','N','A',GETDATE());

insert into usuarios values ('Usuario','npineda','BoXjwFx7aP','Nohora','Pineda Puentes','N','A',GETDATE());

insert into usuarios values ('Usuario','cvelasquez','7vTlo0SbZs','Carlos','Velasquez Garcia','N','A',GETDATE());

insert into usuarios values ('Usuario','cjromero','dqVRd9dNWe','Carlos Julio','Romero','N','A',GETDATE());

insert into usuarios values ('Usuario','jaguiar','kV3YULmVv3','Jose Guillermo','Aguilar Mesa','N','A',GETDATE());

insert into usuarios values ('Usuario','dlara','ypAP2mcbts','Diego Orlando','Lara','N','A',GETDATE());

insert into usuarios values ('Usuario','nbaena','xAOI8i4LWZ','Nancy','Baena Lascano','N','A',GETDATE());


  /* USUARIOS CADES */


  /* INSERT QUERY NO: 1 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JALBINO', '1b2d4IPifK', 'JUAN CAMILO', 'ALBINO MONROY', 'N','A',GETDATE());

/* INSERT QUERY NO: 2 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JALFONSO', 'BorHVqrAon', 'JHON EDWARD YESID', 'ALFONSO MARTINEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 3 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JALVARADO', 'AXbT5AZFrB', 'JESÚS DAVID', 'ALVARADO RODRIGUEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 4 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_AARANGO', 'gcJj3DnKZL', 'ANDRES FELIPE', 'ARANGO HERNANDEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 5 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_MBASTO', 'N7PRc0Axgj', 'MARLY TATIANA', 'BASTO RAMIREZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 6 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JBAUTSITA', 'SaXlQUHFkg', 'JOHANA ALEJANDRA', 'BAUTSITA MEDINA', 'N','A',GETDATE());

/* INSERT QUERY NO: 7 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_VBENITEZ', 'QAdAjb8yAr', 'VALENTINA BENITEZ', 'ALVAREZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 8 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_LBLANCHAR', '4sIkTNDojX', 'LYLY GERALDINE', 'BLANCHAR MARTINEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 9 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_DCANADULCE', 'YPJ7EG5ldl', 'DALLAN PAOLA', 'CAÑADULCE CASTRO', 'N','A',GETDATE());

/* INSERT QUERY NO: 10 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_MCASTAÑEDA', 'gMfZDS07MG', 'MENALY YULIETH', 'CASTAÑEDA ARDILA', 'N','A',GETDATE());

/* INSERT QUERY NO: 11 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JCASTILLO', '3DVcYJtZMt', 'JEFFERSON MIGUEL', 'CASTILLO ARDILA', 'N','A',GETDATE());

/* INSERT QUERY NO: 12 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JDELGADO', '2yQin2SMNJ', 'JASURY ROXANA', 'DELGADO GOMEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 13 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_MFLECHAS', 'L1wSGYaCiG', 'MARIA ALEJANDRA', 'FLECHAS NIÑO', 'N','A',GETDATE());

/* INSERT QUERY NO: 14 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_LGARZON', 'pEfbTapoQT', 'LAURA CAMILA', 'GARZÓN SILVA', 'N','A',GETDATE());

/* INSERT QUERY NO: 15 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_HGONZALEZ', '2aZABiVPEc', 'HEIDY YISEL', 'GONZALEZ SALINAS', 'N','A',GETDATE());

/* INSERT QUERY NO: 16 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_SGUTIERREZ', 'RJ37gpjJAT', 'SARY ELIANA', 'GUTIERREZ CHAVES', 'N','A',GETDATE());

/* INSERT QUERY NO: 17 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_FHERNANDEZ', 'XSU6NlNB5J', 'FABIAN ANDRÉS', 'HERNANDEZ BARRERA', 'N','A',GETDATE());

/* INSERT QUERY NO: 18 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JHERNANDEZ', 'nEdZE65J8A', 'JUAN PABLO', 'HERNANDEZ RAMOS', 'N','A',GETDATE());

/* INSERT QUERY NO: 19 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_SHUERTAS', 'uu6gEcgWPs', 'SILVIA LILIANA', 'HUERTAS MONTAÑA', 'N','A',GETDATE());

/* INSERT QUERY NO: 20 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JIBARRA', 'htfrFUIZDh', 'JONATHAN JAVIER', 'IBARRA ROMERO', 'N','A',GETDATE());

/* INSERT QUERY NO: 21 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_AINFANTE', '1012392653', 'ANDRES FELIPE', 'INFANTE MONTENEGRO', 'N','A',GETDATE());

/* INSERT QUERY NO: 22 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JJAIME', 'iCDTSwVjrR', 'JOHANA MARCELA', 'JAIME CEVAY', 'N','A',GETDATE());

/* INSERT QUERY NO: 23 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JLARA', 'n9FMEDfTUL', 'JEIDY LORENA', 'LARA DIAZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 24 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_XLOZANO', '5bmIUHwIWQ', 'XIMENA LOZANO', 'TRILLERAS', 'N','A',GETDATE());

/* INSERT QUERY NO: 25 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_LMARROQUIN', '0SkiiiN72J', 'LUISA FERNANDA', 'MARROQUIN HERNANDEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 26 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JMEDINA', 'D4DPcU7gyr', 'JUAN CAMILO', 'MEDINA MANRIQUE', 'N','A',GETDATE());

/* INSERT QUERY NO: 27 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_GMENDIETA', '0eJ6TgAKCX', 'GERSON STIVEN', 'MENDIETA FLOREZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 28 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_MMESA', 'InUyuaSLLn', 'MILLER FABIAN', 'MESA RIVERA', 'N','A',GETDATE());

/* INSERT QUERY NO: 29 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_TMONDRAGON', 'XoNYvuXC3M', 'TATIANA GERALDINE', 'MONDRAGON ALZATE', 'N','A',GETDATE());

/* INSERT QUERY NO: 30 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_MMONROY', 'brcpICogpg', 'MARIA RAQUEL', 'MONROY CASTILLO', 'N','A',GETDATE());

/* INSERT QUERY NO: 31 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_LORTIZ', 'AOwGwrFmYf', 'LUISA FERNANDA', 'ORTIZ LLANOS', 'N','A',GETDATE());

/* INSERT QUERY NO: 32 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JORTIZ', 'uK4M5EItiH', 'JORGE ALEJANDRO', 'ORTIZ RODRIGUEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 33 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_LPINZONE', '2Va2M1bXlc', 'LORENA PINZON', 'ESCOBAR', 'N','A',GETDATE());

/* INSERT QUERY NO: 34 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_YPRIETO', 'eYytX9ECIH', 'YEIMI PAOLA', 'PRIETO BAQUERO', 'N','A',GETDATE());

/* INSERT QUERY NO: 35 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_OREINA', 'CJ4E37LqfK', 'OSCAR DAVID', 'REINA RAMIREZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 36 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JRETAVISTA', 'txCO5f5aRg', 'JOHN SEBASTIAN', 'RETAVISTA BALDION', 'N','A',GETDATE());

/* INSERT QUERY NO: 37 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_YROJAS', 'T021jsqth8', 'YENNIFER ROJAS', 'SANDOVAL', 'N','A',GETDATE());

/* INSERT QUERY NO: 38 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JROMERO', 'HkvCTkCZwV', 'JORGE EDUARDO', 'ROMERO', 'N','A',GETDATE());

/* INSERT QUERY NO: 39 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_MRUGE', 'D0rMMyfpQg', 'MIGUEL ANGEL', 'RUGE RUIZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 40 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_LSALINAS', '7tE1hHun1Y', 'LEIDY TATIANA', 'SALINAS ARAGON', 'N','A',GETDATE());

/* INSERT QUERY NO: 41 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_LSINISTERRA', 'b1HT4MPXsS', 'LUZ DARNEYI', 'SINISTERRA MONZON', 'N','A',GETDATE());

/* INSERT QUERY NO: 42 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_KTELLEZ', 'OLFJdqdZsj', 'KAREN LIZBETH', 'TELLEZ RODRIGUEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 43 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_KTORRES', 'N0VXHIETHv', 'KAROL DIANNE', 'TORRES ALFONSO', 'N','A',GETDATE());

/* INSERT QUERY NO: 44 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_STORRES', 'ANkYoW3Rlf', 'SNEIDER ALEXANDER', 'TORRES HENAO', 'N','A',GETDATE());

/* INSERT QUERY NO: 45 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JFTORRES', 'mlZsdX65A7', 'JULIAN FELIPE', 'TORRES NARANJO', 'N','A',GETDATE());

/* INSERT QUERY NO: 46 */
INSERT INTO usuarios VALUES ('Usuario','IDIPRON_JZAMBRANO', 'OJ9sPxvuET', 'JHAN CARLOS', 'ZAMBRANO RODRIGUEZ', 'N','A',GETDATE());

/* INSERT QUERY NO: 47 */
INSERT INTO [SISBEN_S4].[dbo].[usuarios] VALUES ('Usuario','IDIPRON_JCGARZON', 'bqBGt4aTAL', 'JULIO CESAR', 'GARZON', 'N','A',GETDATE());

/* INSERT QUERY NO: 48 */
INSERT INTO [SISBEN_S4].[dbo].[usuarios] VALUES ('Usuario','IDIPRON_DRUIZG', 'MdnUQMF38p', 'DIANA', 'ROCIO RUIZ', 'N','A',GETDATE());