using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SDP_MAESTRA_CONSULTA.Models;
using reCAPTCHA.AspNetCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Mvc;

namespace SDP_MAESTRA_CONSULTA
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MAESTRA_APPContext>(item => item.UseSqlServer(Configuration.GetConnectionString("MaestraDBContext")));
            services.AddRazorPages();
            services.AddHttpClient();

            services.AddMvc().AddRazorPagesOptions(_options =>
            {
                _options.Conventions.ConfigureFilter(new IgnoreAntiforgeryTokenAttribute());
            });

            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.Cookie.Name = ".AdventureWorks.Session";
                options.IdleTimeout = TimeSpan.FromHours(8);
                options.Cookie.IsEssential = true;
            });

            



            // Add recaptcha and pass recaptcha configuration section
            //services.AddRecaptcha(Configuration.GetSection("RecaptchaSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(async (context, next) =>
            {
                //context.Response.Headers.Add("X-Frame-Options", "ALLOW-FROM https://bogotasolidariaencasa.gov.co/");
                //context.Response.Headers.Add("Content-Security-Policy", "frame-ancestors https://bogotasolidariaencasa.gov.co/");
                context.Response.Headers.Add("Content-Security-Policy", "frame-ancestors https://bogotasolidariaencasa.gov.co/ https://rentabasicabogota.gov.co/");
                await next();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
