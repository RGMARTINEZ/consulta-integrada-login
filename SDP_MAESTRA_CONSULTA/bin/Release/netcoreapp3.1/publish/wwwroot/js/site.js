﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

function do_update() {
    var TipoDeVivienda = document.getElementById("TipoDeVivienda").value;

    if (TipoDeVivienda != null && TipoDeVivienda != "Rural") {
        var direccion = "";
        var Via = document.getElementById("Via").value;
        if (Via != null && Via != "") {
            direccion = direccion + Via + " ";
        }
        var NumeroViaPrincipal = document.getElementById("NumeroViaPrincipal").value;
        if (NumeroViaPrincipal != null && NumeroViaPrincipal != "") {
            direccion = direccion + NumeroViaPrincipal + " ";
        }
        var LetraNomenclatura = document.getElementById("LetraNomenclatura").value;
        if (LetraNomenclatura != null && LetraNomenclatura != "") {
            direccion = direccion + LetraNomenclatura + " ";
        }
        var Prefijo = document.getElementById("Prefijo").value;
        if (Prefijo != null && Prefijo != "") {
            direccion = direccion + Prefijo + " ";
        }
        var LetraPrefija = document.getElementById("LetraPrefija").value;
        if (LetraPrefija != null && LetraPrefija != "") {
            direccion = direccion + LetraPrefija + " ";
        }
        var Cuadrante = document.getElementById("Cuadrante").value;
        if (Cuadrante != null && Cuadrante != "") {
            direccion = direccion + Cuadrante + " ";
        }
        var NumeroDeVia = document.getElementById("NumeroDeVia").value;
        if (NumeroDeVia != null && NumeroDeVia != "") {
            direccion = direccion + NumeroDeVia + " ";
        }
        var LetraViaGeneral = document.getElementById("LetraViaGeneral").value;
        if (LetraViaGeneral != null && LetraViaGeneral != "") {
            direccion = direccion + LetraViaGeneral + " ";
        }
        var Subfijo = document.getElementById("Subfijo").value;
        if (Subfijo != null && Subfijo != "") {
            direccion = direccion + Subfijo + " ";
        }
        var LetraPrefijo2 = document.getElementById("LetraPrefijo2").value;
        if (LetraPrefijo2 != null && LetraPrefijo2 != "") {
            direccion = direccion + LetraPrefijo2 + " ";
        }
        var NumeroDePlaca = document.getElementById("NumeroDePlaca").value;
        if (NumeroDePlaca != null && NumeroDePlaca != "") {
            direccion = direccion + NumeroDePlaca + " ";
        }
        var Cuadrante2 = document.getElementById("Cuadrante2").value;
        if (Cuadrante2 != null && Cuadrante2 != "") {
            direccion = direccion + Cuadrante2 + " ";
        }
        document.getElementById("DireccionConcatenada").value = direccion;

    } else {
        document.getElementById("DireccionConcatenada").value = "";
    }    

}


$('[data-toggle="tooltip"]').tooltip()
