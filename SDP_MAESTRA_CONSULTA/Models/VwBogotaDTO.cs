﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class VwBogotaDTO
    {

        public string DirVivienda { get; set; }
        public string NumDocumento { get; set; }
        public string PriNombre { get; set; }
        public string SegNombre { get; set; }
        public string PriApellido { get; set; }
        public string SegApellido { get; set; }
        public string Grupo { get; set; }
        public string Nivel { get; set; }
        public string Clasificacion { get; set; }
        public Int32 Estado { get; set; }
        public string fec_fin_encuesta { get; set; }
        public byte ide_hogar { get; set; }
        public byte ide_persona { get; set; }
        public string EstadoCompleto { get; set; }
        public string CodClase { get; set; }
        public string NomBarrio { get; set; }
        public string NomCorregimiento { get; set; }
        public string NomVereda { get; set; }
        public byte NumCuartosVivienda { get; set; }
        public byte NumHogaresVivienda { get; set; }
        public byte TipVivienda { get; set; }
        public string TipViviendaNombre { get; set; }
        public byte TipMatParedes { get; set; }
        public string TipMatParedesNombre { get; set; }
        public byte TipMatPisos { get; set; }
        public string TipMatPisosNombre { get; set; }
        public string CodComuna { get; set; }
        public byte ind_tiene_alcantarillado { get; set; }
        public string ind_tiene_alcantarillado_Nombre { get; set; }
        public byte ind_tiene_gas { get; set; }
        public string ind_tiene_gas_Nombre { get; set; }
        public byte ind_tiene_recoleccion { get; set; }
        public string ind_tiene_recoleccion_nombre { get; set; }
        public byte ind_tiene_energia { get; set; }
        public string ind_tiene_energia_nombre { get; set; }
        public byte ind_tiene_acueducto { get; set; }
        public string ind_tiene_acueducto_nombre { get; set; }
        public byte tip_estrato_energia { get; set; }
        public byte tip_estrato_acueducto { get; set; }
        public byte NumCuartosUnicosDormir { get; set; }
        public byte NumCuartosDormir { get; set; }
        public byte NumCuartosExclusivos { get; set; }
        public string PriNombreInformante { get; set; }
        public string SegNombreInformante { get; set; }
        public string PriApellidoInformante { get; set; }
        public string SegApellidoInformante { get; set; }
        public string NumTelContacto { get; set; }
        public byte TipoOcupaVivienda { get; set; }
        public string TipoOcupaViviendaNombre { get; set; }
        public byte IndTieneCocina { get; set; }
        public string IndTieneCocinaNombre { get; set; }
        public byte TipOrigenAgua { get; set; }
        public string TipOrigenAguaNombre { get; set; }
        public byte IndAguaLlega7dias { get; set; }
        public string IndAguaLlega7diasNombre { get; set; }
        public byte IndAguaLlega24horas { get; set; }
        public string IndAguaLlega24horasNombre { get; set; }
        public byte TipUsoCocina { get; set; }
        public string TipUsoCocinaNombre { get; set; }
        public byte TipEnergiaCocina { get; set; }
        public string TipEnergiaCocinaNombre { get; set; }
        public byte IndTieneBienRaiz { get; set; }
        public string IndTieneBienRaizNombre { get; set; }
        public byte IndTieneTractor { get; set; }
        public string IndTieneTractorNombre { get; set; }
        public byte IndTieneCarro { get; set; }
        public string IndTieneCarroNombre { get; set; }
        public byte IndTieneMoto { get; set; }
        public string IndTieneMotoNombre { get; set; }
        public byte IndTieneNevera { get; set; }
        public string IndTieneNeveraNombre { get; set; }
        public byte IndTieneLavadora { get; set; }
        public string IndTieneLavadoraNombre { get; set; }
        public byte IndTieneInternet { get; set; }
        public string IndTieneInternetNombre { get; set; }
        public byte IndTienePc { get; set; }
        public string IndTienePcNombre { get; set; }

        public byte ide_informante { get; set; }
        public string ide_informant_nombre { get; set; }

        public Int32 vlr_gasto_alimento { get; set; }
        public Int32 vlr_gasto_salud { get; set; }
        public Int32 vlr_gasto_educacion { get; set; }
        public Int32 vlr_gasto_arriendo { get; set; }
        public Int32 vlr_gasto_serv_publicos { get; set; }
        public Int32 vlr_gasto_celular { get; set; }
        public Int32 vlr_gasto_transporte { get; set; }
        public Int32 vlr_gasto_otros { get; set; }
        public Int32 vlr_total_gastos { get; set; }

        public Int32 EdadCalculada { get; set; }
        public string fec_nacimiento { get; set; }
        public byte SexoPersona { get; set; }
        public string SexoPersonaNombre { get; set; }
        public string CodPaisDocumento { get; set; }
        public string CodPaisDocumentoNombre { get; set; }

        public byte tip_uso_agua_beber { get; set; }
        public string tip_uso_agua_beber_nombre { get; set; }
        public byte tip_prepara_alimentos { get; set; }
        public string tip_prepara_alimentos_nombre { get; set; }
        public byte tip_elimina_basura { get; set; }
        public string tip_elimina_basura_nombre { get; set; }
        public byte tip_sanitario { get; set; }
        public string tip_sanitario_nombre { get; set; }
        public byte tip_ubi_sanitario { get; set; }
        public string tip_ubi_sanitario_nombre { get; set; }
        public byte tip_uso_sanitario { get; set; }
        public string tip_uso_sanitario_nombre { get; set; }

        public byte num_evento_inundacion { get; set; }
        public byte num_evento_terremoto { get; set; }
        public byte num_evento_vendaval { get; set; }
        public byte num_evento_avalancha { get; set; }
        public byte num_evento_incendio { get; set; }
        public byte num_evento_hundimiento { get; set; }

        public byte tip_documento { get; set; }
        public string tip_documento_nombre { get; set; }

        public string cod_dpto_documento { get; set; }
        public string cod_dpto_documentoNombre { get; set; }


        public string cod_mpio_documento { get; set; }
        public string cod_mpio_documentoNombre { get; set; }

        public byte tip_parentesco { get; set; }
        public string tip_parentesco_nombre { get; set; }

        public byte tip_estado_civil { get; set; }
        public string tip_estado_civil_nombre { get; set; }

        public byte ind_conyuge_vive_hogar { get; set; }
        public string ind_conyuge_vive_hogar_nombre { get; set; }

        public byte ind_padre_vive_hogar { get; set; }
        public string ind_padre_vive_hogar_nombre { get; set; }

        public byte tip_seg_social { get; set; }
        public string tip_seg_social_nombre { get; set; }

        public byte ind_enfermo_30 { get; set; }
        public string ind_enfermo_30_nombre { get; set; }

        public byte ind_acudio_salud { get; set; }
        public string ind_acudio_salud_nombre { get; set; }

        public byte ind_fue_atendido_salud { get; set; }
        public string ind_fue_atendido_salud_nombre { get; set; }

        public byte ind_discap_ver { get; set; }
        public string ind_discap_ver_nombre { get; set; }

        public byte ind_discap_oir { get; set; }
        public string ind_discap_oir_nombre { get; set; }

        public byte ind_discap_hablar { get; set; }
        public string ind_discap_hablar_nombre { get; set; }

        public byte ind_discap_moverse { get; set; }
        public string ind_discap_moverse_nombre { get; set; }

        public byte ind_discap_bañarse { get; set; }
        public string ind_discap_bañarse_nombre { get; set; }

        public byte ind_discap_salir { get; set; }
        public string ind_discap_salir_nombre { get; set; }

        public byte ind_discap_entender { get; set; }
        public string ind_discap_entender_nombre { get; set; }

        public byte ind_esta_embarazada { get; set; }
        public string ind_esta_embarazada_nombre { get; set; }

        public byte ind_tuvo_hijos { get; set; }
        public string ind_tuvo_hijos_nombre { get; set; }

        public byte tip_cuidado_niños { get; set; }
        public string tip_cuidado_niños_nombre { get; set; }

        public byte ind_recibe_comida { get; set; }
        public string ind_recibe_comida_nombre { get; set; }

        public byte ind_leer_escribir { get; set; }
        public string ind_leer_escribir_nombre { get; set; }

        public byte ind_estudia { get; set; }
        public string ind_estudia_nombre { get; set; }

        public byte niv_educativo { get; set; }
        public string niv_educativo_nombre { get; set; }

        public byte grado_alcanzado { get; set; }
        public string grado_alcanzado_nombre { get; set; }

        public byte ind_fondo_pensiones { get; set; }
        public string ind_fondo_pensiones_nombre { get; set; }

        public byte tip_empleado { get; set; }
        public string tip_empleado_nombre { get; set; }

        public byte tip_actividad_mes { get; set; }
        public string tip_actividad_mes_nombre { get; set; }

        public Int32 num_sem_buscando { get; set; }
        public string num_sem_buscando_nombre { get; set; }

        public Int32 vlr_ingr_salario { get; set; }
        public Int32 vlr_ingr_honorarios { get; set; }
        public Int32 vlr_ingr_Cosecha { get; set; }
        public Int32 vlr_ingr_pension { get; set; }
        public Int32 vlr_ingr_Remesa_pais { get; set; }
        public Int32 vlr_ingr_Remesa_exterior { get; set; }
        public Int32 vlr_ingr_Arriendos { get; set; }
        public Int32 vlr_otros_ingresos { get; set; }
        public Int32 vlr_ingr_fam_accion { get; set; }
        public Int32 vlr_ingr_col_mayor { get; set; }
        public Int32 vlr_ingr_otro_subsidio { get; set; }








    }
}
