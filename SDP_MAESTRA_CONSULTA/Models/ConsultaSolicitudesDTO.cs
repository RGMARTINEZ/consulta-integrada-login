﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace SDP_MAESTRA_CONSULTA.Models
{
    public class ConsultaSolicitudesDTO
    {
        public string NombreSolicitante { get; set; }
        public string Documento { get; set; }
        public string IdeFichaOrigen { get; set; }
        public string Observaciones { get; set; }
        public string EstadoSolicitud { get; set; }
        public DateTime? FechaReporte { get; set; }
        public string FechaReporteShow { get; set; }
        public Int64 IdSisbenApp { get; set; }
        public string InfoComplementaria { get; set; }
        public Int64 OrdenEstado { get; set; }





    }
}