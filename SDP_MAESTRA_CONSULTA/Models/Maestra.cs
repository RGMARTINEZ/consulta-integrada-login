﻿using System;
using System.Collections.Generic;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class Maestra
    {
        public int Id { get; set; }
        public string Fec_paquete { get; set; }
        public string Cod_mpio { get; set; }
        public string Ide_ficha_origen { get; set; }
        public string Ide_hogar { get; set; }
        public string Ide_persona { get; set; }
        public string Tip_documento { get; set; }
        public string Num_documento { get; set; }
        public string Pri_nombre { get; set; }
        public string Seg_nombre { get; set; }
        public string Pri_apellido { get; set; }
        public string Seg_apellido { get; set; }
        public string Sexo_persona { get; set; }
        public string Fec_nacimiento { get; set; }
        public string Edad_calculada { get; set; }
        public DateTime? Fecha_creacion { get; set; }

    }
}
