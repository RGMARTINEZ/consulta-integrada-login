﻿using System;
using System.Collections.Generic;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class PersonasNoConsolidadoSisben
    {
        public long? CodMpio { get; set; }
        public long? NumPaquete { get; set; }
        public string IdeFichaOrigen { get; set; }
        public long? IdeHogar { get; set; }
        public long? IdePersona { get; set; }
        public string NumDocumento { get; set; }
        public string PriNombre { get; set; }
        public string SegNombre { get; set; }
        public string PriApellido { get; set; }
        public string SegApellido { get; set; }
    }
}
