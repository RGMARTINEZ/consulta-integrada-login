﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class ConsultaSolicitudes
    {
        public string NombreSolicitante { get; set; }
        public string Documento { get; set; }
        public string IdeFichaOrigen { get; set; }
        public string IdSisbenApp { get; set; }

    }
}
