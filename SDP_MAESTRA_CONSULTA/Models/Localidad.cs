﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class Localidad
    {
        public int Id { get; set; }
        public int Codigo_localidad { get; set; }
        public string Nombre { get; set; }
        public string Borrado { get; set; }
        public string Activo { get; set; }
        public DateTime Fecha_creacion { get; set; }
    }
}



