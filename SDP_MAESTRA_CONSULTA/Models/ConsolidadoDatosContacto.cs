﻿using System;
using System.Collections.Generic;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class ConsolidadoDatosContacto
    {
        public int Id { get; set; }
        public string Id_datos_ciudadano { get; set; }
        public string Modo_contacto { get; set; }
        public string Numero { get; set; }
        public string Borrado { get; set; }
        public string Activo { get; set; }
        public string Usuario_creacion { get; set; }    
        public DateTime Fecha_creacion { get; set; }
    }
}
