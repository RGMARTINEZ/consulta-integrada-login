﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class MAESTRA_APPContext : DbContext
    {
        public MAESTRA_APPContext()
        {
        }

        public MAESTRA_APPContext(DbContextOptions<MAESTRA_APPContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Maestra> Maestra { get; set; }
        public virtual DbSet<ConsolidadoDatosCiudadano> ConsolidadoDatosCiudadano { get; set; }
        public virtual DbSet<ConsolidadoDatosVivienda> ConsolidadoDatosVivienda { get; set; }
        public virtual DbSet<ConsolidadoDatosContacto> ConsolidadoDatosContacto { get; set; }
        public virtual DbSet<Localidad> Localidad { get; set; }
        public virtual DbSet<Barrio> Barrio { get; set; }
        public virtual DbSet<Upz> Upz { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<LogInsert> LogInsert { get; set; }

        public virtual DbQuery<ConsolidadoPersonaDTO> ConsolidadoPersonaDTO { get; set; }

        public virtual DbSet<NovedadFichaBogota> NovedadFichaBogota { get; set; }
        public virtual DbSet<PersonasConsolidadoSisben> PersonasConsolidadoSisben { get; set; }
        public virtual DbSet<PersonasNoConsolidadoSisben> PersonasNoConsolidadoSisben { get; set; }
        public virtual DbSet<VwBogota> VwBogota { get; set; }
        public virtual DbSet<ConsultaSolicitudesDTO> ConsultaSolicitudesDTO { get; set; }




        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=10.8.10.40\\\\SUBSIDIO_COVID19,56028;Database=SISBEN_S4;UID=SISBEN_S4_APP;PWD=SisbenS4APP**123;");
               optionsBuilder.UseSqlServer("Server=SDPCLIO33\\SUBSIDIO_COVID19,1433;Database=SISBEN_S4;User ID=SISBEN_S4_APP;Password=Zx123456789*");
                //optionsBuilder.UseSqlServer("Server=SDPCLIO33\\SUBSIDIO_COVID19,1433;Database=SISBEN_S4;User ID=SA;Password=Zx123456789*");


                //optionsBuilder.UseSqlServer("Server=localshost;Database=CONSOLIDADOS;UID=sa;PWD=MiPassw0rd!1521;");

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Maestra>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("consolidados_sisbeniv"); //consolidados_sisbeniv

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Fec_paquete).HasColumnName("fec_paquete");

                entity.Property(e => e.Cod_mpio).HasColumnName("cod_mpio");

                entity.Property(e => e.Ide_ficha_origen).HasColumnName("ide_ficha_origen");

                entity.Property(e => e.Ide_hogar).HasColumnName("ide_hogar");

                entity.Property(e => e.Ide_persona).HasColumnName("ide_persona");

                entity.Property(e => e.Tip_documento).HasColumnName("tip_documento");

                entity.Property(e => e.Num_documento).HasColumnName("num_documento");

                entity.Property(e => e.Pri_nombre).HasColumnName("pri_nombre");

                entity.Property(e => e.Seg_nombre).HasColumnName("seg_nombre");

                entity.Property(e => e.Pri_apellido).HasColumnName("pri_apellido");

                entity.Property(e => e.Seg_apellido).HasColumnName("seg_apellido");

                entity.Property(e => e.Sexo_persona).HasColumnName("sexo_persona");

                entity.Property(e => e.Fec_nacimiento).HasColumnName("fec_nacimiento");

                entity.Property(e => e.Edad_calculada).HasColumnName("edad_calculada");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");
            });


            ////////////////////////////////////////////////////////////////////////////////////////////

            modelBuilder.Entity<ConsolidadoDatosCiudadano>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("consolidado_datos_ciudadano"); //consolidado_datos_ciudadano

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id_tipo_documento).HasColumnName("id_tipo_documento");

                entity.Property(e => e.Numero_documento).HasColumnName("numero_documento");

                entity.Property(e => e.Primer_nombre).HasColumnName("primer_nombre");

                entity.Property(e => e.Segundo_nombre).HasColumnName("segundo_nombre");

                entity.Property(e => e.Primer_apellido).HasColumnName("primer_apellido");

                entity.Property(e => e.Segundo_apellido).HasColumnName("segundo_apellido");

                entity.Property(e => e.Fecha_nacimiento).HasColumnName("fecha_nacimiento");

                entity.Property(e => e.Email).HasColumnName("email");

                entity.Property(e => e.Sexo).HasColumnName("sexo");

                entity.Property(e => e.Borrado).HasColumnName("borrado");
                    
                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Usuario_creacion).HasColumnName("usuario_creacion");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");
            });



            modelBuilder.Entity<ConsolidadoDatosVivienda>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("consolidado_datos_vivienda"); //consolidado_datos_vivienda

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id_datos_ciudadano).HasColumnName("id_datos_ciudadano");

                entity.Property(e => e.Tipo_vivienda).HasColumnName("tipo_vivienda");

                entity.Property(e => e.Tipo_via).HasColumnName("tipo_via");

                entity.Property(e => e.Num_via_principal).HasColumnName("num_via_principal");

                entity.Property(e => e.Letra_nomenclatura).HasColumnName("letra_nomenclatura");

                entity.Property(e => e.Prefijo).HasColumnName("prefijo");

                entity.Property(e => e.Letra_prefijo).HasColumnName("letra_prefijo");

                entity.Property(e => e.Cuadrante).HasColumnName("cuadrante1");

                entity.Property(e => e.Numero_via).HasColumnName("numero_via");

                entity.Property(e => e.Letra_via_genera).HasColumnName("letra_via_genera");

                entity.Property(e => e.Subfijo).HasColumnName("subfijo");

                entity.Property(e => e.Letra_prefijo2).HasColumnName("letra_prefijo2");

                entity.Property(e => e.Numero_placa).HasColumnName("numero_placa");

                entity.Property(e => e.Cuadrante2).HasColumnName("cuadrante2");

                entity.Property(e => e.Direccion_rural).HasColumnName("direccion_rural");

                entity.Property(e => e.Direccion_complemento).HasColumnName("direccion_complemento");

                entity.Property(e => e.Direccion_concatenada).HasColumnName("direccion_concatenada");

                entity.Property(e => e.Localidad).HasColumnName("localidad");

                entity.Property(e => e.Barrio).HasColumnName("barrio");

                entity.Property(e => e.Upz).HasColumnName("upz");

                entity.Property(e => e.Borrado).HasColumnName("borrado");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Usuario_creacion).HasColumnName("usuario_creacion");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");
            });




            modelBuilder.Entity<ConsolidadoDatosContacto>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("consolidado_datos_contacto"); //consolidado_datos_contacto

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id_datos_ciudadano).HasColumnName("id_datos_ciudadano");

                entity.Property(e => e.Modo_contacto).HasColumnName("modo_contacto");

                entity.Property(e => e.Numero).HasColumnName("numero");

                entity.Property(e => e.Borrado).HasColumnName("borrado");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Usuario_creacion).HasColumnName("usuario_creacion");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");

            });

            modelBuilder.Entity<Localidad>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("tipo_localidad"); //tipo_localidad

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Codigo_localidad).HasColumnName("codigo_localidad");

                entity.Property(e => e.Nombre).HasColumnName("nombre");

                entity.Property(e => e.Borrado).HasColumnName("borrado");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");

            });

            modelBuilder.Entity<Barrio>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("tipo_barrio"); //tipo_barrio

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Tipo_localidad).HasColumnName("tipo_localidad");

                entity.Property(e => e.Codigo_barrio).HasColumnName("codigo_barrio");

                entity.Property(e => e.Borrado).HasColumnName("nombre");

                entity.Property(e => e.Borrado).HasColumnName("borrado");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");

            });

            modelBuilder.Entity<Upz>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("tipo_upz"); //tipo_upz

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Tipo_localidad).HasColumnName("tipo_localidad");

                entity.Property(e => e.Codigo_upz).HasColumnName("codigo_upz");

                entity.Property(e => e.Nombre).HasColumnName("nombre");

                entity.Property(e => e.Borrado).HasColumnName("borrado");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");

            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("usuarios"); //tipo_upz

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Tipo).HasColumnName("tipo");

                entity.Property(e => e.Username).HasColumnName("username");

                entity.Property(e => e.Password).HasColumnName("password");

                entity.Property(e => e.Borrado).HasColumnName("borrado");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");

            });

            modelBuilder.Entity<LogInsert>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("log_inserts"); //consolidado_datos_ciudadano

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Id_tipo_documento).HasColumnName("id_tipo_documento");

                entity.Property(e => e.Numero_documento).HasColumnName("numero_documento");

                entity.Property(e => e.Primer_nombre).HasColumnName("primer_nombre");

                entity.Property(e => e.Segundo_nombre).HasColumnName("segundo_nombre");

                entity.Property(e => e.Primer_apellido).HasColumnName("primer_apellido");

                entity.Property(e => e.Segundo_apellido).HasColumnName("segundo_apellido");

                entity.Property(e => e.Borrado).HasColumnName("borrado");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Usuario_creacion).HasColumnName("usuario_creacion");

                entity.Property(e => e.Fecha_creacion).HasColumnName("fecha_creacion");
            });


            ////////////////////////////////////////////////////////////////////////////////////////////

            modelBuilder.Entity<NovedadFichaBogota>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("NOVEDAD_FICHA_BOGOTA");

                entity.Property(e => e.CodMpioCns).HasColumnName("COD_MPIO_CNS");

                entity.Property(e => e.CodMpioProc).HasColumnName("COD_MPIO_PROC");

                entity.Property(e => e.DesNovedad).HasColumnName("DES_NOVEDAD");

                entity.Property(e => e.DetNovedadCns).HasColumnName("DET_NOVEDAD_CNS");

                entity.Property(e => e.DetNovedadProc).HasColumnName("DET_NOVEDAD_PROC");

                entity.Property(e => e.IdeFichaOrigenCns)
                    .HasColumnName("IDE_FICHA_ORIGEN_CNS")
                    .HasMaxLength(50);

                entity.Property(e => e.IdeFichaOrigenProc)
                    .HasColumnName("IDE_FICHA_ORIGEN_PROC")
                    .HasMaxLength(50);

                entity.Property(e => e.NumNovedad).HasColumnName("NUM_NOVEDAD");

                entity.Property(e => e.NumPaqueteProc).HasColumnName("NUM_PAQUETE_PROC");
            });

            modelBuilder.Entity<PersonasConsolidadoSisben>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PERSONAS_CONSOLIDADO_SISBEN");

                entity.Property(e => e.CodMpio).HasColumnName("COD_MPIO");

                entity.Property(e => e.EdadCalculada).HasColumnName("EDAD_CALCULADA");

                entity.Property(e => e.FecNacimiento)
                    .HasColumnName("FEC_NACIMIENTO")
                    .HasColumnType("date");

                entity.Property(e => e.FecPaquete)
                    .HasColumnName("FEC_PAQUETE")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdeFichaOrigen)
                    .HasColumnName("IDE_FICHA_ORIGEN")
                    .HasMaxLength(50);

                entity.Property(e => e.IdeHogar).HasColumnName("IDE_HOGAR");

                entity.Property(e => e.IdePersona).HasColumnName("IDE_PERSONA");

                entity.Property(e => e.NumDocumento)
                    .HasColumnName("NUM_DOCUMENTO")
                    .HasMaxLength(50);

                entity.Property(e => e.PriApellido)
                    .HasColumnName("PRI_APELLIDO")
                    .HasMaxLength(50);

                entity.Property(e => e.PriNombre)
                    .HasColumnName("PRI_NOMBRE")
                    .HasMaxLength(50);

                entity.Property(e => e.SegApellido)
                    .HasColumnName("SEG_APELLIDO")
                    .HasMaxLength(50);

                entity.Property(e => e.SegNombre)
                    .HasColumnName("SEG_NOMBRE")
                    .HasMaxLength(50);

                entity.Property(e => e.SexoPersona).HasColumnName("SEXO_PERSONA");

                entity.Property(e => e.TipDocumento).HasColumnName("TIP_DOCUMENTO");
            });

            modelBuilder.Entity<PersonasNoConsolidadoSisben>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("PERSONAS_NO_CONSOLIDADO_SISBEN");

                entity.Property(e => e.CodMpio).HasColumnName("COD_MPIO");

                entity.Property(e => e.IdeFichaOrigen)
                    .HasColumnName("IDE_FICHA_ORIGEN")
                    .HasMaxLength(50);

                entity.Property(e => e.IdeHogar).HasColumnName("IDE_HOGAR");

                entity.Property(e => e.IdePersona).HasColumnName("IDE_PERSONA");

                entity.Property(e => e.NumDocumento)
                    .HasColumnName("NUM_DOCUMENTO")
                    .HasMaxLength(50);

                entity.Property(e => e.NumPaquete).HasColumnName("NUM_PAQUETE");

                entity.Property(e => e.PriApellido)
                    .HasColumnName("PRI_APELLIDO")
                    .HasMaxLength(50);

                entity.Property(e => e.PriNombre)
                    .HasColumnName("PRI_NOMBRE")
                    .HasMaxLength(50);

                entity.Property(e => e.SegApellido)
                    .HasColumnName("SEG_APELLIDO")
                    .HasMaxLength(50);

                entity.Property(e => e.SegNombre)
                    .HasColumnName("SEG_NOMBRE")
                    .HasMaxLength(50);
            });


            modelBuilder.Entity<VwBogota>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("VW_BOGOTA");

                entity.Property(e => e.DirVivienda)
                   .HasColumnName("DIR_VIVIENDA")
                   .HasMaxLength(250);

                entity.Property(e => e.NumDocumento)
                    .HasColumnName("NUM_DOCUMENTO")
                    .HasMaxLength(50);


                entity.Property(e => e.PriApellido)
                    .HasColumnName("PRI_APELLIDO")
                    .HasMaxLength(50);

                entity.Property(e => e.PriNombre)
                    .HasColumnName("PRI_NOMBRE")
                    .HasMaxLength(50);

                entity.Property(e => e.SegApellido)
                    .HasColumnName("SEG_APELLIDO")
                    .HasMaxLength(50);

                entity.Property(e => e.SegNombre)
                    .HasColumnName("SEG_NOMBRE")
                    .HasMaxLength(50);

                entity.Property(e => e.CodClase)
                    .HasColumnName("Cod_clase")
                    .HasMaxLength(50);

                entity.Property(e => e.NomBarrio)
                     .HasColumnName("NOM_BARRIO")
                     .HasMaxLength(100);

                entity.Property(e => e.NomCorregimiento)
                     .HasColumnName("NOM_CORREGIMIENTO")
                     .HasMaxLength(100);

                entity.Property(e => e.NumCuartosVivienda)
                    .HasColumnName("NUM_CUARTOS_VIVIENDA")
                    .HasMaxLength(50);

                entity.Property(e => e.NumHogaresVivienda)
                    .HasColumnName("NUM_HOGARES_VIVIENDA")
                    .HasMaxLength(50);

                entity.Property(e => e.NomVereda)
                    .HasColumnName("NOM_VEREDA")
                    .HasMaxLength(50);

                entity.Property(e => e.TipVivienda)
                    .HasColumnName("TIP_VIVIENDA")
                    .HasMaxLength(50);

                entity.Property(e => e.TipMatParedes)
                    .HasColumnName("TIP_MAT_PAREDES")
                    .HasMaxLength(50);

                entity.Property(e => e.TipMatPisos)
                    .HasColumnName("TIP_MAT_PISOS")
                    .HasMaxLength(50);

                entity.Property(e => e.CodComuna)
                    .HasColumnName("COD_COMUNA")
                    .HasMaxLength(50);



                entity.Property(e => e.NumCuartosUnicosDormir)
                    .HasColumnName("NUM_CUARTOS_UNICOS_DORMIR")
                    .HasMaxLength(50);

                entity.Property(e => e.NumCuartosDormir)
                    .HasColumnName("NUM_CUARTOS_DORMIR")
                    .HasMaxLength(50);

                entity.Property(e => e.NumCuartosExclusivos)
                    .HasColumnName("NUM_CUARTOS_EXCLUSIVOS")
                    .HasMaxLength(50);


                entity.Property(e => e.PriNombreInformante)
                    .HasColumnName("PRI_NOM_INFORMANTE")
                    .HasMaxLength(50);

                entity.Property(e => e.PriApellidoInformante)
                    .HasColumnName("PRI_APE_INFORMANTE")
                    .HasMaxLength(50);

                entity.Property(e => e.SegNombreInformante)
                    .HasColumnName("SEG_NOM_INFORMANTE")
                    .HasMaxLength(50);

                entity.Property(e => e.SegApellidoInformante)
                    .HasColumnName("SEG_APE_INFORMANTE")
                    .HasMaxLength(50);

                entity.Property(e => e.NumTelContacto)
                    .HasColumnName("NUM_TEL_CONTACTO")
                    .HasMaxLength(50);


                entity.Property(e => e.TipoOcupaVivienda)
                    .HasColumnName("TIP_OCUPA_VIVIENDA")
                    .HasMaxLength(50);


                entity.Property(e => e.IndTieneCocina)
                    .HasColumnName("IND_TIENE_COCINA")
                    .HasMaxLength(50);

                entity.Property(e => e.TipOrigenAgua)
                    .HasColumnName("TIP_ORIGEN_AGUA")
                    .HasMaxLength(50);

                entity.Property(e => e.IndAguaLlega7dias)
                    .HasColumnName("IND_AGUA_LLEGA_7DIAS")
                    .HasMaxLength(50);

                entity.Property(e => e.IndAguaLlega24horas)
                    .HasColumnName("IND_AGUA_LLEGA_24HORAS")
                    .HasMaxLength(50);

                entity.Property(e => e.TipUsoCocina)
                    .HasColumnName("TIP_USO_COCINA")
                    .HasMaxLength(50);


                entity.Property(e => e.TipEnergiaCocina)
                    .HasColumnName("TIP_ENERGIA_COCINA")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTieneBienRaiz)
                    .HasColumnName("IND_TIENE_BIEN_RAIZ")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTieneTractor)
                    .HasColumnName("IND_TIENE_TRACTOR")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTieneCarro)
                    .HasColumnName("IND_TIENE_CARRO")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTieneMoto)
                    .HasColumnName("IND_TIENE_MOTO")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTieneNevera)
                    .HasColumnName("IND_TIENE_NEVERA")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTieneLavadora)
                    .HasColumnName("IND_TIENE_LAVADORA")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTieneInternet)
                    .HasColumnName("IND_TIENE_INTERNET")
                    .HasMaxLength(50);

                entity.Property(e => e.IndTienePc)
                    .HasColumnName("IND_TIENE_PC")
                    .HasMaxLength(50);

                entity.Property(e => e.EdadCalculada)
                    .HasColumnName("EDAD_CALCULADA")
                    .HasMaxLength(50);

                entity.Property(e => e.SexoPersona)
                    .HasColumnName("SEXO_PERSONA")
                    .HasMaxLength(50);

                entity.Property(e => e.CodPaisDocumento)
                    .HasColumnName("COD_PAIS_DOCUMENTO")
                    .HasMaxLength(50);

            });


            modelBuilder.Entity<ConsultaSolicitudesDTO>(entity =>
            {

                entity.HasNoKey();

                entity.ToTable("VW_CONSULTA_DETALLE_SOLICITUD_ENCUESTA");

                entity.Property(e => e.NombreSolicitante).HasColumnName("nombre_solicitante");
                entity.Property(e => e.Documento).HasColumnName("documento");
                entity.Property(e => e.IdSisbenApp).HasColumnName("id_sol_sisben_app");
                entity.Property(e => e.IdeFichaOrigen).HasColumnName("num_ficha");
                entity.Property(e => e.EstadoSolicitud).HasColumnName("listado");
                entity.Property(e => e.OrdenEstado).HasColumnName("orden_listado");

                entity.Property(e => e.FechaReporte).HasColumnName("fecha_inicio_etapa");
                entity.Property(e => e.Observaciones).HasColumnName("observaciones");
                entity.Property(e => e.InfoComplementaria).HasColumnName("info_complementaria");




            });








            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
