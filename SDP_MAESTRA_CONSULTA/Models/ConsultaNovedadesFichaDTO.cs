﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class ConsultaNovedadesFichaDTO
    {
        public string NUM_NOVEDAD { get; set; }
        public string COD_MPIO_PROC { get; set; }
        public string NUM_PAQUETE_PROC { get; set; }
        public string IDE_FICHA_ORIGEN_PROC { get; set; }
        public string DES_NOVEDAD { get; set; }
        public string DET_NOVEDAD_PROC { get; set; }
        public string COD_MPIO_CNS { get; set; }
        public string IDE_FICHA_ORIGEN_CNS { get; set; }
        public string DET_NOVEDAD_CNS { get; set; }
    }
}
