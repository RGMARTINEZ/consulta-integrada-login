﻿using System;
using System.Collections.Generic;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class ConsolidadoDatosCiudadano
    {
        public int Id { get; set; }
        public string Id_tipo_documento { get; set; }
        public string Numero_documento { get; set; }
        public string Primer_nombre { get; set; }
        public string Segundo_nombre { get; set; }
        public string Primer_apellido { get; set; }
        public string Segundo_apellido { get; set; }
        public string Fecha_nacimiento { get; set; }
        public string Email { get; set; }
        public string Sexo { get; set; }
        public string Borrado { get; set; }
        public string Activo { get; set; }
        public string Usuario_creacion { get; set; }
        public DateTime Fecha_creacion { get; set; }
    }
}
