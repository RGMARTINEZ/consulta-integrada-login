﻿using System;
using System.Collections.Generic;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class NovedadFichaBogota
    {
        public long? NumNovedad { get; set; }
        public long? CodMpioProc { get; set; }
        public long? NumPaqueteProc { get; set; }
        public string IdeFichaOrigenProc { get; set; }
        public string DesNovedad { get; set; }
        public string DetNovedadProc { get; set; }
        public long? CodMpioCns { get; set; }
        public string IdeFichaOrigenCns { get; set; }
        public string DetNovedadCns { get; set; }
    }
}
