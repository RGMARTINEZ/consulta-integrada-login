﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class Usuario
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Borrado { get; set; }
        public string Activo { get; set; }
        public DateTime Fecha_creacion { get; set; }
    }
}
