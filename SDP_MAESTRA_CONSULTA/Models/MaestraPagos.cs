﻿using System;
using System.Collections.Generic;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class MaestraPagos
    {
        public string IdLlaveMaestra { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string NumeroDocumento { get; set; }
        public int? FechaExpedicionDocumento { get; set; }
        public string Vigencia { get; set; }
        public string Ciclo { get; set; }
        public int? PerteneceHogarConPagoCiclo { get; set; }
        public int? PersonaPagadaCiclo { get; set; }
        public int? EsGiro { get; set; }
        //public int? EsGiroCobrado { get; set; }
        public string ListaPago { get; set; }
        public string FechaCiclo { get; set; }
        public string MontoPago { get; set; }
        public string OperadorFinanciero { get; set; }
    }
}
