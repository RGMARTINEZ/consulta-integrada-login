﻿using System;
using System.Collections.Generic;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class ConsolidadoDatosVivienda
    {
        public int Id { get; set; }
        public string Id_datos_ciudadano { get; set; }
        public string Tipo_vivienda { get; set; }
        public string Tipo_via { get; set; }
        public string Num_via_principal { get; set; }
        public string Letra_nomenclatura { get; set; }
        public string Prefijo { get; set; }
        public string Letra_prefijo { get; set; }
        public string Cuadrante { get; set; }
        public string Numero_via { get; set; }
        public string Letra_via_genera { get; set; }
        public string Subfijo { get; set; }
        public string Letra_prefijo2 { get; set; }
        public string Numero_placa { get; set; }
        public string Cuadrante2 { get; set; }
        public string Direccion_rural { get; set; }
        public string Direccion_complemento { get; set; }
        public string Direccion_concatenada { get; set; }
        public string Localidad { get; set; }
        public string Barrio { get; set; }
        public string Upz { get; set; }
        public string Borrado { get; set; }
        public string Activo { get; set; }
        public string Usuario_creacion { get; set; }
        public DateTime Fecha_creacion { get; set; }
    }
}
