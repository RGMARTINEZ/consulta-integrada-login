﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class ConsolidadoPersonaVistaSisbenDTO
    {
        public string NUM_DOCUMENTO { get; set; }
        public string PRI_NOMBRE { get; set; }
        public string SEG_NOMBRE { get; set; }
        public string PRI_APELLIDO { get; set; }
        public string SEG_APELLIDO { get; set; }
        public string COD_COMUNA { get; set; }
        public string COD_BARRIO { get; set; }
        public string NOM_BARRIO { get; set; }
        public string DIR_VIVIENDA { get; set; }
        public string NUM_TEL_CONTACTO { get; set; }
        public string EMAIL_CONTACTO { get; set; }
        public string FEC_NACIMIENTO { get; set; }
        public string EDAD_CALCULADA { get; set; }
        public string EDAD_CALCULADA_HOY { get; set; }
        public string SEXO_PERSONA { get; set; }
        public string TIP_PARENTESCO { get; set; }
        public string IDE_FICHA_ORIGEN { get; set; }
        public string GRUPO { get; set; }
        public string NIVEL { get; set; }
        public string FEC_INI_ENCUESTA { get; set; }
        public string FEC_DIGITACION { get; set; }
        public string COORD_X_MANUAL_REC { get; set; }
        public string COORD_Y_MANUAL_REC { get; set; }
        public string COORD_X_AUTO_REC { get; set; }
        public string COORD_Y_AUTO_REC { get; set; }
        public string COORD_X_MANUAL_ENC { get; set; }
        public string COORD_Y_MANUAL_ENC { get; set; }
        public string COORD_X_AUTO_ENC { get; set; }
        public string COORD_Y_AUTO_ENC { get; set; }
        public Int32 ESTADO { get; set; }
        public DateTime? FEC_FIN_ENCUESTA { get; set; }
        public byte IDE_HOGAR { get; set; }
        public byte IDE_PERSONA { get; set; }



    }
}
