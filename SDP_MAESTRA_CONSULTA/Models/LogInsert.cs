﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class LogInsert
    {
        public int Id { get; set; }
        public string Id_tipo_documento { get; set; }
        public string Numero_documento { get; set; }
        public string Primer_nombre { get; set; }
        public string Segundo_nombre { get; set; }
        public string Primer_apellido { get; set; }
        public string Segundo_apellido { get; set; }
        public string Borrado { get; set; }
        public string Activo { get; set; }
        public string Usuario_creacion { get; set; }
        public DateTime Fecha_creacion { get; set; }
    }
}
