﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class VwBogotaDetalleDTO
    {
        public string DirVivienda { get; set; }
        public string NumDocumento { get; set; }
        public string PriNombre { get; set; }
        public string SegNombre { get; set; }
        public string PriApellido { get; set; }
        public string SegApellido { get; set; }
        public string Grupo { get; set; }
        public string Nivel { get; set; }
        public string Clasificacion { get; set; }
        public Int32 Estado { get; set; }
        public string fec_fin_persona1 { get; set; }
        public byte ide_hogar { get; set; }
        public byte ide_persona { get; set; }
        public string EstadoCompleto { get; set; }




    }
}
