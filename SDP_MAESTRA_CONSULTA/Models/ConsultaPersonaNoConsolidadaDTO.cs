﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SDP_MAESTRA_CONSULTA.Models
{
    public partial class ConsultaPersonaNoConsolidadaDTO
    {
        public string COD_MPIO { get; set; }
        public string IDE_FICHA_ORIGEN { get; set; }
        public string IDE_HOGAR { get; set; }
        public string IDE_PERSONA { get; set; }
        public string NUM_DOCUMENTO { get; set; }
        public string PRI_NOMBRE { get; set; }
        public string SEG_NOMBRE { get; set; }
        public string PRI_APELLIDO { get; set; }
        public string SEG_APELLIDO { get; set; }
    }
}
