﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using reCAPTCHA.AspNetCore.Attributes;
using SDP_MAESTRA_CONSULTA.Models;
using Microsoft.Data.SqlClient;
using System.Text;

namespace SDP_MAESTRA_CONSULTA.Pages.Consulta
{
    public class IndexModel : PageModel
    {
        private readonly SDP_MAESTRA_CONSULTA.Models.MAESTRA_APPContext _context;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IHttpClientFactory _clientFactory;


        [Display(Name = "Número de ficha *")]
        [BindProperty(SupportsGet = true)]
        [Required]
        public string NumeroFicha { get; set; }

        [Display(Name = "Número de documento *")]
        [BindProperty(SupportsGet = true)]
        [Required]
        public string NumeroDocumento { get; set; }

        [Display(Name = "Primer Nombre")]
        [BindProperty(SupportsGet = true)]
        [Required]
        public string PrimerNombre { get; set; }

        [Display(Name = "Primer Apellido")]
        [BindProperty(SupportsGet = true)]
        [Required]
        public string PrimerApellido { get; set; }

        [Display(Name = "Segundo Nombre")]
        [BindProperty(SupportsGet = true)]
        public string SegundoNombre { get; set; }

        [Display(Name = "Segundo Apellido")]
        [BindProperty(SupportsGet = true)]
        public string SegundoApellido { get; set; }

        public int entro = 0;
        public Maestra persona = new Maestra();
        public MaestraDTO personaDTO = new MaestraDTO();

        public Boolean MostrarError { get; set; }
        public Boolean MostrarValidacion { get; set; }

        public List<ConsolidadoPersonaDTO> listadoConsolidadoPersona { get; set; }
        public List<ConsolidadoPersonaDTO> listadoNoConsolidadoPersona { get; set; }
        public List<NovedadFichaBogota> listadoNovedad { get; set; }
        public List<VwBogotaDTO> listadoVistaBog { get; set; }

        public List<ConsultaSolicitudesDTO> listadoSolicitudes { get; set; }


        public int countConsolidadoPersona { get; set; }
        public int countNoConsolidadoPersona { get; set; }
        public int countNovedad { get; set; }
        public int countBogota { get; set; }

        public IndexModel(SDP_MAESTRA_CONSULTA.Models.MAESTRA_APPContext context, ILogger<IndexModel> logger, IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _context = context;
            _configuration = configuration;
            _logger = logger;
            _clientFactory = clientFactory;
        }

        public IActionResult OnGet()
        {
            var username = HttpContext.Session.GetString("UserName");
            if (username != null && !username.Equals(""))
            {
                //HttpContext.Session.SetString("UserName", username);
            }
            else
            {
                return RedirectToPage("Login");
            }
            return Page();
        }


        [Obsolete]
        public async Task<IActionResult> OnPostAsync()
        {
            var username = HttpContext.Session.GetString("UserName");

            if (username != null && !username.Equals(""))
            {
                countConsolidadoPersona = 0;
                countNoConsolidadoPersona = 0;
                countNovedad = 0;
                countBogota = 0;
                var numeroFicha = string.IsNullOrWhiteSpace(NumeroFicha) ? "" : NumeroFicha.Trim();
                var numeroDocumento = string.IsNullOrWhiteSpace(NumeroDocumento) ? "" : NumeroDocumento.ToUpper().Trim();
                var primerNombre = string.IsNullOrWhiteSpace(PrimerNombre) ? "" : PrimerNombre.ToUpper().Trim();
                var primerApellido = string.IsNullOrWhiteSpace(PrimerApellido) ? "" : PrimerApellido.ToUpper().Trim();
                var segundoNombre = string.IsNullOrWhiteSpace(SegundoNombre) ? "" : SegundoNombre.ToUpper().Trim();
                var segundoApellido = string.IsNullOrWhiteSpace(SegundoApellido) ? ""  : SegundoApellido.ToUpper().Trim();  

                MostrarValidacion = false;

                if (ValidarFormulario(numeroFicha, numeroDocumento, primerNombre, primerApellido, segundoNombre, segundoApellido))
                {
                    try
                    {

                        ///  PRIMERA CONSULTA

                        String sqlLimpioConsolidadoPersona = "";
                        StringBuilder sqlConsolidadoPersona = new StringBuilder();
                        sqlConsolidadoPersona.Append("SELECT DISTINCT *  \n");
                        sqlConsolidadoPersona.Append("From  PERSONAS_CONSOLIDADO_SISBEN \n");
                        sqlConsolidadoPersona.Append("Where  \n");

                        var paramConsolidadoPersona = new SqlParameter[] { };

                        if (numeroFicha != null && !numeroFicha.Equals(""))
                            sqlConsolidadoPersona.Append(" IDE_FICHA_ORIGEN = '" + numeroFicha + "'  AND\n");

                        if (numeroDocumento != null && !numeroDocumento.Equals(""))
                            sqlConsolidadoPersona.Append(" NUM_DOCUMENTO = '" + numeroDocumento + "'  AND\n");

                        if (primerNombre != null && !primerNombre.Equals(""))
                            sqlConsolidadoPersona.Append(" PRI_NOMBRE LIKE '%" + primerNombre + "%' AND\n");

                        if (primerApellido != null && !primerApellido.Equals(""))
                            sqlConsolidadoPersona.Append(" PRI_APELLIDO LIKE '%" + primerApellido + "%' AND\n");

                        if (segundoNombre != null && !segundoNombre.Equals(""))
                            sqlConsolidadoPersona.Append(" SEG_NOMBRE LIKE '%" + segundoNombre + "%' AND\n");

                        if (segundoApellido != null && !segundoApellido.Equals(""))
                            sqlConsolidadoPersona.Append(" SEG_APELLIDO LIKE '%" + segundoApellido + "%' AND\n");

                        sqlLimpioConsolidadoPersona = sqlConsolidadoPersona.ToString().Substring(0, sqlConsolidadoPersona.Length - 5);

                        listadoConsolidadoPersona = await _context.PersonasConsolidadoSisben.FromSql(sqlLimpioConsolidadoPersona, paramConsolidadoPersona).Select(s => new ConsolidadoPersonaDTO
                        {
                            idCount = countConsolidadoPersona,
                            FecPaquete = s.FecPaquete,
                            CodMpio = s.CodMpio,
                            IdeFichaOrigen = s.IdeFichaOrigen,
                            IdeHogar = s.IdeHogar,
                            IdePersona = s.IdePersona,
                            NumDocumento = s.NumDocumento,
                            PriNombre = s.PriNombre,
                            SegNombre = s.SegNombre,
                            PriApellido = s.PriApellido,
                            SegApellido = s.SegApellido,
                            SexoPersona = s.SexoPersona,
                            FecNacimiento = s.FecNacimiento,
                            EdadCalculada = s.EdadCalculada,
                            NombreCompleto = (s.PriNombre + " " +  (string.IsNullOrWhiteSpace(s.SegNombre) ? "" : s.SegNombre.Trim()) ).Trim() + " " + (s.PriApellido + " " + (string.IsNullOrWhiteSpace(s.SegApellido) ? "" : s.SegApellido.Trim())).Trim(),
                            DateCompleto = (s.FecNacimiento).ToString()
                        
                        }).Distinct().ToListAsync();

                        ///  SOLICITUDES CONSULTA

                        String sqlSolicitudesPersona= "";
                        StringBuilder sqlSolicitudes = new StringBuilder();
                        sqlSolicitudes.Append("SELECT *  \n");
                        sqlSolicitudes.Append("From  [SISBEN_S4].[dbo].VW_CONSULTA_DETALLE_SOLICITUD_ENCUESTA \n");
                        sqlSolicitudes.Append("Where  \n");

                        var paramConsultaSolicitudes = new SqlParameter[] { };

                        if (numeroFicha != null && !numeroFicha.Equals(""))
                            sqlSolicitudes.Append(" NUM_FICHA = '" + numeroFicha + "'  AND\n");

                        if (numeroDocumento != null && !numeroDocumento.Equals(""))
                            sqlSolicitudes.Append(" DOCUMENTO = '" + numeroDocumento + "'  AND\n");/*

                        if (numeroDocumento != null && !numeroDocumento.Equals(""))
                            sqlSolicitudes.Append(" NOMBRE_SOLICITANTE = '" + primerNombre + ' ' + primerApellido + ' ' + segundoNombre + ' ' + segundoApellido + "'  AND\n");*/

                        /* if (primerNombre != null && !primerNombre.Equals(""))
                             sqlConsolidadoPersona.Append(" NOMBRE_SOLICITANTE LIKE '%" + primerNombre + ' ' + segundoNombre + ' ' + primerApellido + ' ' + segundoApellido + "%' \n");

                         if (primerNombre != null && !primerNombre.Equals(""))
                             sqlConsolidadoPersona.Append(" NOMBRE_SOLICITANTE LIKE '%" + primerNombre + "%' AND\n");

                         if (primerApellido != null && !primerApellido.Equals(""))
                             sqlConsolidadoPersona.Append(" NOMBRE_SOLICITANTE LIKE '%" + primerApellido + "%' AND\n");

                         if (segundoNombre != null && !segundoNombre.Equals(""))
                             sqlConsolidadoPersona.Append(" NOMBRE_SOLICITANTE LIKE '%" + segundoNombre + "%' AND\n");

                         if (segundoApellido != null && !segundoApellido.Equals(""))
                             sqlConsolidadoPersona.Append(" NOMBRE_SOLICITANTE LIKE '%" + segundoApellido + "%' AND\n");*/



                        sqlSolicitudesPersona = sqlSolicitudes.ToString().Substring(0, sqlSolicitudes.Length - 5);

                        Console.WriteLine(sqlSolicitudesPersona, "QUERRRRRRRYYYYYYYYYYYYYYYYY");


                        listadoSolicitudes = await _context.ConsultaSolicitudesDTO.FromSql(sqlSolicitudesPersona, paramConsultaSolicitudes).Select(s => new ConsultaSolicitudesDTO
                        {
                            IdSisbenApp = s.IdSisbenApp,
                            IdeFichaOrigen = s.IdeFichaOrigen,
                            Documento = s.Documento,
                            NombreSolicitante = s.NombreSolicitante,
                            EstadoSolicitud = s.EstadoSolicitud,
                            FechaReporteShow = s.FechaReporte.ToString(),
                            Observaciones = s.Observaciones,
                            OrdenEstado = s.OrdenEstado,
                            InfoComplementaria = s.InfoComplementaria







                        }).Distinct().OrderByDescending(s => s.IdSisbenApp).ThenByDescending(s => s.FechaReporteShow).ToListAsync();

                        //Console.WriteLine(listadoSolicitudes, "SOLICITUDESSSSSSSSSSS");



                        /// CONSULTA PERSONA ENCUESTA
                        /*
                        String sqlEstadosSolicitudes = "";
                        StringBuilder sqlSolicitudes = new StringBuilder();
                        SqlCommand consultaPersonaEncuesta = new SqlCommand("SP_CONSULTA_PERSONA_SOLICITUD_ENCUESTA");

                        consultaPersonaEncuesta.Parameters.Add("@NOMBRE_1", System.Data.SqlDbType.NVarChar, 50).Value = "YENI";
                        consultaPersonaEncuesta.Parameters.Add("@NOMBRE_2", System.Data.SqlDbType.NVarChar, 50).Value = "MARIBEL";
                        consultaPersonaEncuesta.Parameters.Add("@APELLIDO_1", System.Data.SqlDbType.NVarChar, 50).Value = "ALBINO";
                        consultaPersonaEncuesta.Parameters.Add("@APELLIDO_2", System.Data.SqlDbType.NVarChar, 50).Value = "MORENO";
                        consultaPersonaEncuesta.Parameters.Add("@NO_DOCUMENTO", System.Data.SqlDbType.NVarChar, 50).Value = 1002565589;
                        consultaPersonaEncuesta.Parameters.Add("@NUM_FICHA", System.Data.SqlDbType.NVarChar, 50).Value = 11001588599700020311;

                        consultaPersonaEncuesta.ExecuteNonQuery();


                        // sqlEstadosSolicitudes = InsertStockInformation.ToString();




                        ///  ESTADOS CONSULTA HSTORICO

                        String sqlEstadosSolicitudesHistorico = "";
                        SqlCommand consultaHistoricos = new SqlCommand("SP_CONSULTA_HISTORICO_SOLICITUDES");

                        consultaHistoricos.Parameters.Add("@idSolicitud", System.Data.SqlDbType.Int).Value = 119455;
                        consultaHistoricos.Parameters.Add("@ficha", System.Data.SqlDbType.NVarChar, 50).Value = 11001588599700020311;

                        consultaHistoricos.ExecuteNonQuery();

                        */
                        // sqlEstadosSolicitudes = InsertStockInformation.ToString();





                        ///  SEGUNDA CONSULTA

                        String sqlLimpioNoConsolidadoPersona = "";
                        StringBuilder sqlNoConsolidadoPersona = new StringBuilder();

                        sqlNoConsolidadoPersona.Append("SELECT *  \n");
                        sqlNoConsolidadoPersona.Append("From  PERSONAS_NO_CONSOLIDADO_SISBEN \n");
                        sqlNoConsolidadoPersona.Append("Where  \n");

                        var paramNoConsolidadoPersona = new SqlParameter[] { };

                        if (numeroFicha != null && !numeroFicha.Equals(""))
                            sqlNoConsolidadoPersona.Append(" IDE_FICHA_ORIGEN = '" + numeroFicha + "'  AND\n");

                        if (numeroDocumento != null && !numeroDocumento.Equals(""))
                            sqlNoConsolidadoPersona.Append(" NUM_DOCUMENTO = '" + numeroDocumento + "'  AND\n");

                        if (primerNombre != null && !primerNombre.Equals(""))
                            sqlNoConsolidadoPersona.Append(" PRI_NOMBRE LIKE '%" + primerNombre + "%' AND\n");

                        if (primerApellido != null && !primerApellido.Equals(""))
                            sqlNoConsolidadoPersona.Append(" PRI_APELLIDO LIKE '%" + primerApellido + "%' AND\n");

                        if (segundoNombre != null && !segundoNombre.Equals(""))
                            sqlNoConsolidadoPersona.Append(" SEG_NOMBRE LIKE '%" + segundoNombre + "%' AND\n");

                        if (segundoApellido != null && !segundoApellido.Equals(""))
                            sqlNoConsolidadoPersona.Append(" SEG_APELLIDO LIKE '%" + segundoApellido + "%' AND\n");

                        sqlLimpioNoConsolidadoPersona = sqlNoConsolidadoPersona.ToString().Substring(0, sqlNoConsolidadoPersona.Length - 5);

                        listadoNoConsolidadoPersona = await _context.PersonasNoConsolidadoSisben.FromSql(sqlLimpioNoConsolidadoPersona, paramNoConsolidadoPersona).Select(s => new ConsolidadoPersonaDTO
                        {
                            idCount = countConsolidadoPersona,
                            CodMpio = s.CodMpio,
                            IdeFichaOrigen = s.IdeFichaOrigen,
                            IdeHogar = s.IdeHogar,
                            IdePersona = s.IdePersona,
                            NumDocumento = s.NumDocumento,
                            PriNombre = s.PriNombre,
                            SegNombre = s.SegNombre,
                            PriApellido = s.PriApellido,
                            SegApellido = s.SegApellido,
                            NombreCompleto = (s.PriNombre + " " + (string.IsNullOrWhiteSpace(s.SegNombre) ? "" : s.SegNombre.Trim())).Trim() + " " + (s.PriApellido + " " + (string.IsNullOrWhiteSpace(s.SegApellido) ? "" : s.SegApellido.Trim())).Trim()
                        }).Distinct().ToListAsync();

                        ///  TERCERA CONSULTA


                        String sqlLimpioNovedad = "";
                        StringBuilder sqlNovedad = new StringBuilder();
                        sqlNovedad.Append("SELECT DISTINCT *  \n");
                        sqlNovedad.Append("From  NOVEDAD_FICHA_BOGOTA \n");
                        sqlNovedad.Append("Where  \n");

                        var paramNovedad = new SqlParameter[] { };
                    
                        if (numeroFicha != null && !numeroFicha.Equals(""))
                            sqlNovedad.Append(" (IDE_FICHA_ORIGEN_PROC = '" + numeroFicha + "' OR IDE_FICHA_ORIGEN_CNS = '" + numeroFicha + "' )   OR\n");

                        if ((numeroDocumento != null && !numeroDocumento.Equals(""))
                            || (primerNombre != null && !primerNombre.Equals(""))
                            || (primerApellido != null && !primerApellido.Equals(""))
                            || (segundoNombre != null && !segundoNombre.Equals(""))
                            || (segundoApellido != null && !segundoApellido.Equals(""))){

                            sqlNovedad.Append(" DET_NOVEDAD_PROC LIKE '");

                            if (numeroDocumento != null && !numeroDocumento.Equals(""))
                                sqlNovedad.Append("%" + numeroDocumento);

                            if (primerNombre != null && !primerNombre.Equals(""))
                                sqlNovedad.Append("%" + primerNombre);

                            if (primerApellido != null && !primerApellido.Equals(""))
                                sqlNovedad.Append("%" + primerApellido);

                            if (segundoNombre != null && !segundoNombre.Equals(""))
                                sqlNovedad.Append("%" + segundoNombre);

                            if (segundoApellido != null && !segundoApellido.Equals(""))
                                sqlNovedad.Append("%" + segundoApellido);

                            sqlNovedad.Append("%'   OR\n");
                        }

                        if ((numeroDocumento != null && !numeroDocumento.Equals(""))
                            || (primerNombre != null && !primerNombre.Equals(""))
                            || (primerApellido != null && !primerApellido.Equals(""))
                            || (segundoNombre != null && !segundoNombre.Equals(""))
                            || (segundoApellido != null && !segundoApellido.Equals("")))
                        {

                            sqlNovedad.Append(" DET_NOVEDAD_CNS LIKE '");

                            if (numeroDocumento != null && !numeroDocumento.Equals(""))
                                sqlNovedad.Append("%" + numeroDocumento);

                            if (primerNombre != null && !primerNombre.Equals(""))
                                sqlNovedad.Append("%" + primerNombre);

                            if (primerApellido != null && !primerApellido.Equals(""))
                                sqlNovedad.Append("%" + primerApellido);

                            if (segundoNombre != null && !segundoNombre.Equals(""))
                                sqlNovedad.Append("%" + segundoNombre);

                            if (segundoApellido != null && !segundoApellido.Equals(""))
                                sqlNovedad.Append("%" + segundoApellido);

                            sqlNovedad.Append("%'   OR\n");
                        }

                        sqlLimpioNovedad = sqlNovedad.ToString().Substring(0, sqlNovedad.Length - 5);

                        listadoNovedad = await _context.NovedadFichaBogota.FromSql(sqlLimpioNovedad, paramNovedad).Distinct().ToListAsync();


                        ///  CUARTA CONSULTA


                        String sqlLimpioVistaBog = "";
                        StringBuilder sqlVistaBog  = new StringBuilder();
                        sqlVistaBog.Append("SELECT IDE_FICHA_ORIGEN, NUM_DOCUMENTO, PRI_NOMBRE, PRI_APELLIDO, SEG_NOMBRE, SEG_APELLIDO, DIR_VIVIENDA, GRUPO, NIVEL, CLASIFICACION, ESTADO, FEC_FIN_ENCUESTA, IDE_HOGAR, IDE_PERSONA, Cod_clase, NOM_BARRIO, NOM_CORREGIMIENTO, NUM_CUARTOS_VIVIENDA, NUM_HOGARES_VIVIENDA, NOM_VEREDA, TIP_VIVIENDA, TIP_MAT_PAREDES, TIP_MAT_PISOS, COD_COMUNA,  \n");
                        sqlVistaBog.Append("NUM_CUARTOS_UNICOS_DORMIR, NUM_CUARTOS_DORMIR, NUM_CUARTOS_EXCLUSIVOS, PRI_NOM_INFORMANTE, PRI_APE_INFORMANTE, SEG_NOM_INFORMANTE, SEG_APE_INFORMANTE, NUM_TEL_CONTACTO, TIP_OCUPA_VIVIENDA, IND_TIENE_COCINA,TIP_ORIGEN_AGUA, IND_AGUA_LLEGA_7DIAS, IND_AGUA_LLEGA_24HORAS, \n");
                        sqlVistaBog.Append("TIP_USO_COCINA, TIP_ENERGIA_COCINA, IND_TIENE_BIEN_RAIZ, IND_TIENE_TRACTOR, IND_TIENE_CARRO, IND_TIENE_MOTO, IND_TIENE_NEVERA, IND_TIENE_LAVADORA, IND_TIENE_INTERNET, IND_TIENE_PC, EDAD_CALCULADA, FEC_NACIMIENTO, SEXO_PERSONA, COD_PAIS_DOCUMENTO, TIP_USO_AGUA_BEBER, TIP_PREPARA_ALIMENTOS, TIP_ELIMINA_BASURA, TIP_SANITARIO, TIP_UBI_SANITARIO, TIP_USO_SANITARIO, \n");
                        sqlVistaBog.Append("IND_TIENE_ALCANTARILLADO, IND_TIENE_GAS, IND_TIENE_RECOLECCION, IND_TIENE_ENERGIA, IND_TIENE_ACUEDUCTO, TIP_ESTRATO_ENERGIA, TIP_ESTRATO_ACUEDUCTO, VLR_GASTO_ALIMENTO, VLR_GASTO_SALUD, VLR_GASTO_EDUCACION, VLR_GASTO_ARRIENDO, VLR_GASTO_SERV_PUBLICOS, VLR_GASTO_CELULAR, VLR_GASTO_TRANSPORTE, VLR_GASTO_OTROS, VLR_TOTAL_GASTOS, NUM_EVENTO_INUNDACION, NUM_EVENTO_TERREMOTO, NUM_EVENTO_VENDAVAL, \n");
                        sqlVistaBog.Append("NUM_EVENTO_AVALANCHA, NUM_EVENTO_INCENDIO, NUM_EVENTO_HUNDIMIENTO, TIP_DOCUMENTO, COD_DPTO_DOCUMENTO, COD_MPIO_DOCUMENTO, TIP_PARENTESCO, TIP_ESTADO_CIVIL, IND_CONYUGE_VIVE_HOGAR, IND_PADRE_VIVE_HOGAR, TIP_SEG_SOCIAL, IND_ENFERMO_30, IND_ACUDIO_SALUD, IND_FUE_ATENDIDO_SALUD, IND_DISCAP_VER, IND_DISCAP_OIR, IND_DISCAP_HABLAR, IND_DISCAP_MOVERSE, \n");
                        sqlVistaBog.Append("IND_DISCAP_BAÑARSE, IND_DISCAP_SALIR, IND_DISCAP_ENTENDER, IND_ESTA_EMBARAZADA, IND_TUVO_HIJOS, TIP_CUIDADO_NIÑOS, IND_RECIBE_COMIDA, IND_LEER_ESCRIBIR, IND_ESTUDIA, NIV_EDUCATIVO, GRADO_ALCANZADO, IND_FONDO_PENSIONES, TIP_EMPLEADO, TIP_ACTIVIDAD_MES, NUM_SEM_BUSCANDO, VLR_INGR_SALARIO, VLR_INGR_HONORARIOS, VLR_INGR_COSECHA, VLR_INGR_PENSION, VLR_INGR_REMESA_PAIS, VLR_INGR_REMESA_EXTERIOR, VLR_INGR_ARRIENDOS, \n");
                        sqlVistaBog.Append("VLR_OTROS_INGRESOS, VLR_INGR_FAM_ACCION, VLR_INGR_COL_MAYOR, VLR_INGR_OTRO_SUBSIDIO, IDE_INFORMANTE \n");


                        // sqlVistaBog.Append("From  [10.10.18.52].[DDS_SISBEN4_REPORTES].[DBO].[VW_BOGOTA] \n");
                        sqlVistaBog.Append("From  BD_SISBEN_IV_DNP \n");

                        sqlVistaBog.Append("Where  \n");

                        var paramVistaBog = new SqlParameter[] { };

                        if (numeroFicha != null && !numeroFicha.Equals(""))
                            sqlVistaBog.Append(" IDE_FICHA_ORIGEN = '" + numeroFicha + "'  AND\n");

                        if (numeroDocumento != null && !numeroDocumento.Equals(""))
                            sqlVistaBog.Append(" NUM_DOCUMENTO = '" + numeroDocumento + "'  AND\n");

                        if (primerNombre != null && !primerNombre.Equals(""))
                            sqlVistaBog.Append(" PRI_NOMBRE LIKE '%" + primerNombre + "%' AND\n");

                        if (primerApellido != null && !primerApellido.Equals(""))
                            sqlVistaBog.Append(" PRI_APELLIDO LIKE '%" + primerApellido + "%' AND\n");

                        if (segundoNombre != null && !segundoNombre.Equals(""))
                            sqlVistaBog.Append(" SEG_NOMBRE LIKE '%" + segundoNombre + "%' AND\n");

                        if (segundoApellido != null && !segundoApellido.Equals(""))
                            sqlVistaBog.Append(" SEG_APELLIDO LIKE '%" + segundoApellido + "%' AND\n");

                        sqlLimpioVistaBog = sqlVistaBog.ToString().Substring(0, sqlVistaBog.Length - 5);

                        listadoVistaBog = await _context.VwBogota.FromSql(sqlLimpioVistaBog, paramVistaBog).Select(s => new VwBogotaDTO
                        {
                            Clasificacion = s.Clasificacion,
                            Estado = s.Estado,
                            Grupo = s.Grupo,
                            DirVivienda = s.DirVivienda,
                            ide_hogar = s.ide_hogar,
                            ide_persona = s.ide_persona,
                            Nivel = s.Nivel,
                            NumDocumento = s.NumDocumento,
                            PriApellido = s.PriApellido,
                            PriNombre = s.PriNombre,
                            SegApellido = s.SegApellido,
                            SegNombre = s.SegNombre,
                            fec_fin_encuesta = s.fec_fin_encuesta.ToString(),

                            fec_nacimiento = s.fec_nacimiento.ToString(),

                            CodClase = s.CodClase,
                            NomBarrio = s.NomBarrio,
                            NomCorregimiento = s.NomCorregimiento,
                            NumCuartosVivienda = s.NumCuartosVivienda,
                            NumHogaresVivienda = s.NumHogaresVivienda,
                            NomVereda = s.NomVereda,
                            TipVivienda = s.TipVivienda,
                            TipMatParedes = s.TipMatParedes,
                            TipMatPisos = s.TipMatPisos,
                            CodComuna = s.CodComuna,
                            NumCuartosUnicosDormir = s.NumCuartosUnicosDormir,
                            NumCuartosDormir = s.NumCuartosDormir,
                            NumCuartosExclusivos = s.NumCuartosExclusivos,
                            PriNombreInformante = s.PriNombreInformante,
                            PriApellidoInformante = s.PriApellidoInformante,
                            SegNombreInformante = s.SegNombreInformante,
                            SegApellidoInformante = s.SegApellidoInformante,
                            NumTelContacto = s.NumTelContacto,
                            TipoOcupaVivienda = s.TipoOcupaVivienda,
                            IndTieneCocina = s.IndTieneCocina,
                            TipOrigenAgua = s.TipOrigenAgua,
                            IndAguaLlega7dias = s.IndAguaLlega7dias,
                            IndAguaLlega24horas = s.IndAguaLlega24horas,

                            TipUsoCocina = s.TipUsoCocina,
                            TipEnergiaCocina = s.TipEnergiaCocina,
                            IndTieneBienRaiz = s.IndTieneBienRaiz,
                            IndTieneTractor = s.IndTieneTractor,
                            IndTieneCarro = s.IndTieneCarro,
                            IndTieneMoto = s.IndTieneMoto,
                            IndTieneNevera = s.IndTieneNevera,
                            IndTieneLavadora = s.IndTieneLavadora,
                            IndTieneInternet = s.IndTieneInternet,
                            IndTienePc = s.IndTienePc,

                            EdadCalculada = s.EdadCalculada,
                            CodPaisDocumento = s.CodPaisDocumento,

                            SexoPersona = s.SexoPersona,

                            tip_uso_agua_beber = s.tip_uso_agua_beber,
                            tip_prepara_alimentos = s.tip_prepara_alimentos,
                            tip_elimina_basura = s.tip_elimina_basura,
                            tip_sanitario = s.tip_sanitario,
                            tip_ubi_sanitario = s.tip_ubi_sanitario,
                            tip_uso_sanitario = s.tip_uso_sanitario,


                            ind_tiene_alcantarillado = s.ind_tiene_alcantarillado,
                            ind_tiene_gas = s.ind_tiene_gas,
                            ind_tiene_recoleccion = s.ind_tiene_recoleccion,
                            ind_tiene_energia = s.ind_tiene_energia,
                            ind_tiene_acueducto = s.ind_tiene_acueducto,
                            tip_estrato_energia = s.tip_estrato_energia,
                            tip_estrato_acueducto = s.tip_estrato_acueducto,


                            vlr_gasto_alimento = s.vlr_gasto_alimento,
                            vlr_gasto_salud = s.vlr_gasto_salud,
                            vlr_gasto_educacion = s.vlr_gasto_educacion,
                            vlr_gasto_arriendo = s.vlr_gasto_arriendo,
                            vlr_gasto_serv_publicos = s.vlr_gasto_serv_publicos,
                            vlr_gasto_celular = s.vlr_gasto_celular,
                            vlr_gasto_transporte = s.vlr_gasto_transporte,
                            vlr_gasto_otros = s.vlr_gasto_otros,
                            vlr_total_gastos = s.vlr_total_gastos,


                            num_evento_inundacion = s.num_evento_inundacion,
                            num_evento_terremoto = s.num_evento_terremoto,
                            num_evento_vendaval = s.num_evento_vendaval,
                            num_evento_avalancha = s.num_evento_avalancha,
                            num_evento_incendio = s.num_evento_incendio,
                            num_evento_hundimiento = s.num_evento_hundimiento,


                            tip_documento = s.tip_documento,
                            cod_dpto_documento = s.cod_dpto_documento,
                            cod_mpio_documento = s.cod_mpio_documento,
                            tip_parentesco = s.tip_parentesco,
                            tip_estado_civil = s.tip_estado_civil,
                            ind_conyuge_vive_hogar = s.ind_conyuge_vive_hogar,
                            ind_padre_vive_hogar = s.ind_padre_vive_hogar,
                            tip_seg_social = s.tip_seg_social,
                            ind_enfermo_30 = s.ind_enfermo_30,
                            ind_acudio_salud = s.ind_acudio_salud,
                            ind_fue_atendido_salud = s.ind_fue_atendido_salud,
                            ind_discap_ver = s.ind_discap_ver,
                            ind_discap_oir = s.ind_discap_oir,
                            ind_discap_hablar = s.ind_discap_hablar,
                            ind_discap_moverse = s.ind_discap_moverse,
                            ind_discap_bañarse = s.ind_discap_bañarse,
                            ind_discap_salir = s.ind_discap_salir,
                            ind_discap_entender = s.ind_discap_entender,
                            ind_esta_embarazada = s.ind_esta_embarazada,
                            ind_tuvo_hijos = s.ind_tuvo_hijos,
                            tip_cuidado_niños = s.tip_cuidado_niños,
                            ind_recibe_comida = s.ind_recibe_comida,
                            ind_leer_escribir = s.ind_leer_escribir,
                            ind_estudia = s.ind_estudia,
                            niv_educativo = s.niv_educativo,
                            grado_alcanzado = s.grado_alcanzado,
                            ind_fondo_pensiones = s.ind_fondo_pensiones,
                            tip_empleado = s.tip_empleado,
                            tip_actividad_mes = s.tip_actividad_mes,
                            num_sem_buscando = s.num_sem_buscando,

                            vlr_ingr_salario = s.vlr_ingr_salario,
                            vlr_ingr_honorarios = s.vlr_ingr_honorarios,
                            vlr_ingr_Cosecha = s.vlr_ingr_Cosecha,
                            vlr_ingr_pension = s.vlr_ingr_pension,
                            vlr_ingr_Remesa_pais = s.vlr_ingr_Remesa_pais,
                            vlr_ingr_Remesa_exterior = s.vlr_ingr_Remesa_exterior,
                            vlr_ingr_Arriendos = s.vlr_ingr_Arriendos,
                            vlr_otros_ingresos = s.vlr_otros_ingresos,
                            vlr_ingr_fam_accion = s.vlr_ingr_fam_accion,
                            vlr_ingr_col_mayor = s.vlr_ingr_col_mayor,
                            vlr_ingr_otro_subsidio = s.vlr_ingr_otro_subsidio,

                            ide_informante = s.ide_informante,




                        }).Distinct().OrderBy(s => s.ide_hogar).ThenBy(s => s.ide_persona).ToListAsync();

                        foreach (VwBogotaDTO lst in listadoVistaBog)
                        {
                            if(lst.fec_fin_encuesta!= null)
                            {
                                DateTime hoy = DateTime.Parse(lst.fec_fin_encuesta);
                                String anio = "" + hoy.Year;
                                String mes = "" + hoy.Month;
                                String dia = "" + hoy.Day;
                                lst.fec_fin_encuesta = anio + "-" + ((int.Parse(mes) < 10)? "0"+mes : mes) + "-" + ((int.Parse(dia) < 10)? "0"+dia:dia);


                                DateTime hoyN = DateTime.Parse(lst.fec_nacimiento);
                                String anioN = "" + hoyN.Year;
                                String mesN = "" + hoyN.Month;
                                String diaN = "" + hoyN.Day;
                                lst.fec_nacimiento = anioN + "-" + ((int.Parse(mesN) < 10) ? "0" + mesN : mesN) + "-" + ((int.Parse(diaN) < 10) ? "0" + diaN : diaN);

                                lst.EstadoCompleto = tipoDocumentoStrNuevo(lst.Estado);
                                lst.CodClase = getClaseId(lst.CodClase);
                                lst.TipViviendaNombre = getTipoViviendaNombre(lst.TipVivienda.ToString());
                                lst.TipMatParedesNombre = getTipoMaterialParedesNombre(lst.TipMatParedes.ToString());
                                lst.TipMatPisosNombre = getTipoPisosNombre(lst.TipMatPisos.ToString());

                                lst.IndTieneCocinaNombre = getEstadosSINO(lst.IndTieneCocina.ToString());
                                lst.IndAguaLlega7diasNombre = getEstadosSINO(lst.IndAguaLlega7dias.ToString());
                                lst.IndAguaLlega24horasNombre = getEstadosSINO(lst.IndAguaLlega24horas.ToString());

                                lst.IndAguaLlega24horasNombre = getEstadosSINO(lst.IndAguaLlega24horas.ToString());
                                lst.IndTieneBienRaizNombre = getEstadosSINO(lst.IndTieneBienRaiz.ToString());
                                lst.IndTieneTractorNombre = getEstadosSINO(lst.IndTieneTractor.ToString());
                                lst.IndTieneCarroNombre = getEstadosSINO(lst.IndTieneCarro.ToString());
                                lst.IndTieneMotoNombre = getEstadosSINO(lst.IndTieneMoto.ToString());
                                lst.IndTieneNeveraNombre = getEstadosSINO(lst.IndTieneNevera.ToString());
                                lst.IndTieneLavadoraNombre = getEstadosSINO(lst.IndTieneLavadora.ToString());
                                lst.IndTieneInternetNombre = getEstadosSINO(lst.IndTieneInternet.ToString());
                                lst.IndTienePcNombre = getEstadosSINO(lst.IndTienePc.ToString());
                                lst.SexoPersonaNombre = getSexo(lst.SexoPersona.ToString());
                                lst.TipoOcupaViviendaNombre = getOcupacionVivienda(lst.TipoOcupaVivienda.ToString());
                                lst.TipOrigenAguaNombre = getTipoOrigenAgua(lst.TipOrigenAgua.ToString());
                                lst.TipUsoCocinaNombre = getUsoCocina(lst.TipUsoCocina.ToString());
                                lst.TipEnergiaCocinaNombre = getTipoEnergia(lst.TipEnergiaCocina.ToString());
                                lst.tip_uso_agua_beber_nombre = getTipoUsoAguaBeber(lst.tip_uso_agua_beber.ToString());
                                lst.tip_prepara_alimentos_nombre = getPreparaAlimento(lst.tip_prepara_alimentos.ToString());
                                lst.tip_elimina_basura_nombre = getEliminarBasura(lst.tip_elimina_basura.ToString());
                                lst.tip_sanitario_nombre = getTipoSanitario(lst.tip_sanitario.ToString());
                                lst.tip_ubi_sanitario_nombre = getUbicacionSanitario(lst.tip_ubi_sanitario.ToString());
                                lst.tip_uso_sanitario_nombre = getUsoSanitario(lst.tip_uso_sanitario.ToString());
                                lst.CodPaisDocumentoNombre = getPaisDocumento(lst.CodPaisDocumento.ToString());
                                lst.cod_dpto_documentoNombre = getDptoDocumento(lst.cod_dpto_documento.ToString());

                                lst.cod_mpio_documentoNombre = getMcpioDocumento(lst.cod_mpio_documento.ToString());



                                lst.ind_tiene_alcantarillado_Nombre = getEstadosSINO(lst.ind_tiene_alcantarillado.ToString());
                                lst.ind_tiene_gas_Nombre = getEstadosSINO(lst.ind_tiene_gas.ToString());
                                lst.ind_tiene_recoleccion_nombre = getEstadosSINO(lst.ind_tiene_recoleccion.ToString());
                                lst.ind_tiene_energia_nombre = getEstadosSINO(lst.ind_tiene_energia.ToString());
                                lst.ind_tiene_acueducto_nombre = getEstadosSINO(lst.ind_tiene_acueducto.ToString());


                                lst.tip_documento_nombre = tipoDocumento(lst.tip_documento.ToString());
                                lst.tip_parentesco_nombre = getParentesco(lst.tip_parentesco.ToString());
                                lst.tip_estado_civil_nombre = getEstadoCivil(lst.tip_estado_civil.ToString());
                                lst.ind_conyuge_vive_hogar_nombre = getEstadosSINOFLUJO(lst.ind_conyuge_vive_hogar.ToString());
                                lst.ind_padre_vive_hogar_nombre = getEstadosSINO(lst.ind_padre_vive_hogar.ToString());
                                lst.tip_seg_social_nombre = getSeguridadSocial(lst.tip_seg_social.ToString());
                                lst.ind_enfermo_30_nombre = getEstadosSINO(lst.ind_enfermo_30.ToString());
                                lst.ind_acudio_salud_nombre = getEstadosSINOFLUJO(lst.ind_acudio_salud.ToString());
                                lst.ind_fue_atendido_salud_nombre = getEstadosSINOFLUJO(lst.ind_fue_atendido_salud.ToString());
                                lst.ind_discap_ver_nombre = getEstadosSINO(lst.ind_discap_ver.ToString());
                                lst.ind_discap_oir_nombre = getEstadosSINO(lst.ind_discap_oir.ToString());
                                lst.ind_discap_hablar_nombre = getEstadosSINO(lst.ind_discap_hablar.ToString());
                                lst.ind_discap_moverse_nombre = getEstadosSINO(lst.ind_discap_moverse.ToString());





                                lst.ind_discap_bañarse_nombre = getEstadosSINO(lst.ind_discap_bañarse.ToString());
                                lst.ind_discap_salir_nombre = getEstadosSINO(lst.ind_discap_salir.ToString());
                                lst.ind_discap_entender_nombre = getEstadosSINO(lst.ind_discap_entender.ToString());
                                lst.ind_esta_embarazada_nombre = getEstadosSINOFLUJO(lst.ind_esta_embarazada.ToString());
                                lst.ind_tuvo_hijos_nombre = getEstadosSINOFLUJO(lst.ind_tuvo_hijos.ToString());
                                lst.tip_cuidado_niños_nombre = getCuidadoNinos(lst.tip_cuidado_niños.ToString());
                                lst.ind_recibe_comida_nombre = getEstadosSINOFLUJO(lst.ind_recibe_comida.ToString());
                                lst.ind_leer_escribir_nombre = getEstadosSINOFLUJO(lst.ind_leer_escribir.ToString());
                                lst.ind_estudia_nombre = getEstadosSINOFLUJO(lst.ind_estudia.ToString());
                                lst.niv_educativo_nombre = getNivelEducativo(lst.niv_educativo.ToString());
                                lst.ind_fondo_pensiones_nombre = getFondoPensiones(lst.ind_fondo_pensiones.ToString());
                                lst.tip_empleado_nombre = getEmpleado(lst.tip_empleado.ToString());
                                lst.tip_actividad_mes_nombre = getActividadMes(lst.tip_actividad_mes.ToString());
                                lst.num_sem_buscando_nombre = getSemenasTrabajo(lst.num_sem_buscando.ToString());





                            }
                        }

                        if ((listadoConsolidadoPersona != null && listadoConsolidadoPersona.Count > 0) 
                            || (listadoNoConsolidadoPersona != null && listadoNoConsolidadoPersona.Count > 0)
                            || (listadoNovedad != null && listadoNovedad.Count > 0)
                            || (listadoVistaBog != null && listadoVistaBog.Count > 0))
                        {
                            MostrarValidacion = true;
                            MostrarError = false;
                            ModelState.Clear();

                            LogInsert log = new LogInsert();
                            log.Id_tipo_documento = ""+numeroFicha;
                            log.Numero_documento = ""+numeroDocumento;
                            log.Primer_nombre = primerNombre;
                            log.Segundo_nombre = segundoNombre;
                            log.Primer_apellido = primerApellido;
                            log.Segundo_apellido = segundoApellido;
                            log.Borrado = "N";
                            log.Activo = "A";
                            log.Usuario_creacion = username;
                            log.Fecha_creacion = DateTime.Now;

                            _context.LogInsert.Add(log);
                            await _context.SaveChangesAsync();

                        }
                        else
                        {
                            MostrarValidacion = false;
                            MostrarError = true;
                            ModelState.Clear();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        MostrarError = true;
                        MostrarValidacion = false;
                        ModelState.Clear();
                    }
                
                }      
                else
                {
                    if (entro == 0)
                    {
                        entro++;
                    }
                    else
                    {
                        MostrarValidacion = true;
                        MostrarError = false;
                    }

                }
            }
            else
            {
                return Redirect("~/Consulta/Login");
            }

            return Page();
        }


        public IActionResult OnPostDelete()
        {
            HttpContext.Session.SetString("UserName", "");
            return Redirect("~/Consulta/Login");
        }


        public IActionResult OnPostLimpiar()
        {
            NumeroFicha = "";
            NumeroDocumento = "";
            PrimerNombre = "";
            PrimerApellido = "";
            SegundoNombre = "";
            SegundoApellido = "";
            return Redirect("~/Consulta/Index");
        }


        public static string CreateDBDateTime(string date)
        {
            DateTime result;
            if (DateTime.TryParse(date, out result))
            {
                return result.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            return "";
        }



        /// <summary>
        ///  Metodo que valida el formulario
        /// </summary>
        /// <param name="numeroFicha"></param>
        /// <param name="numeroDocumento"></param>
        /// <param name="primerNombre"></param>
        /// <param name="primerApellido"></param>
        /// <param name="segundoNombre"></param>
        /// <param name="segundoApellido"></param>
        /// <returns></returns>
        private Boolean ValidarFormulario(string numeroFicha, string numeroDocumento, string primerNombre, 
            string primerApellido, string segundoNombre, string segundoApellido)
        {
            if (numeroFicha == null && numeroDocumento == null && primerNombre == null && primerApellido == null 
                && segundoNombre == null && segundoApellido == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }




        private string tipoDocumentoStr(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
            return "RC";


            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
            return "TI";


            if (id.Equals("3"))
                //return "Cédula de Ciudadanía";
            return "CC";


            if (id.Equals("4"))
                //return "Cédula de Extranjería";
            return "CE";


            if (id.Equals("5"))
                //return "DNI (país de origen)";
            return "DNI";


            if (id.Equals("6"))
                //return "Pasaporte";
            return "PAS";

            if (id.Equals("7"))
                //return "Salvoconducto Migratorio";
            return "SM";


            if (id.Equals("8"))
                //return "Permiso Especial de Permanencia";
            return "PEP";


            return null;
        }

        private string tipoDocumentoStrNuevo(int id)
        {
            if (id.Equals(0))
                return "Registro Valido";

            if (id.Equals(10))
                return "Excluido - Fallecido";

            if (id.Equals(30))
                return "Verificación - jefe de hogar -  Por Denuncia";

            if (id.Equals(31))
                return "Verificación - miembros del hogar -  Por Denuncia";

            if (id.Equals(40))
                return "Verificación - Desactualización de la información - Sección E. Salud y fecundida - ind_esta_embarazada";

            if (id.Equals(41))
                return "Verificación - Desactualización de la información - Sección G. Educación - niv_educativo";

            if (id.Equals(42))
                return "Verificación - Desactualización de la información - Sección G. Educación - Ind_fondo_pensiones";

            if (id.Equals(43))
                return "Verificación - Desactualización de la información - Sección H. Ocupación e ingresos ";

            if (id.Equals(60))
                return "Verificación - Calidad de la encuesta - Datos no validos en la encuesta (imposible calcular grupo y nivel)";

            if (id.Equals(633))
                return "Excluido - Calidad de la encuesta - Datos no validos en la encuesta (fechas de encuesta)";

            if (id.Equals(61))
                return "Verificación - Calidad de la encuesta - Parentescos inconsistentes";

            if (id.Equals(62))
                return "Verificación - Calidad de la encuesta - Sección G. Educación - grado_alcanzado";

            if (id.Equals(63))
                return "Verificación - Calidad de la encuesta - Sección D. Antecedentes Sociodemograficos";

            if (id.Equals(64))
                return "Verificación - Calidad de la encuesta - Sección E. Salud y fecundidad - ind_esta_embarazada - ind_tuvo_hijos";

            if (id.Equals(65))
                return "Verificación - Calidad de la encuesta - Sección H. Ocupación e ingresos";

            if (id.Equals(66))
                return "Verificación - Calidad de la encuesta - Sección B. Vivienda";

            if (id.Equals(67))
                return "Verificación - Calidad de la encuesta -  Sección C. Hogares";

            if (id.Equals(68))
                return "Verificación - Calidad de la encuesta -  Sección C. Hogares - num_cuartos_vivienda";

            if (id.Equals(70))
                return "Excluido - Calidad de la encuesta - Documento no valido";

            if (id.Equals(71))
                return "Excluido - Calidad de la encuesta - Numero documento duplicado";

            if (id.Equals(72))
                return "Excluido - Calidad de la encuesta - Documento no valido Hogas sin posible jefe";

            if (id.Equals(90))
                return "Verificación - Calidad del Registro - Desmejoramiento en variables de vivienda";

            if (id.Equals(91))
                return "Verificación - Calidad del Registro - Desmejoramiento en variables de nivel educativo";




            return null;
        }

        private string getClaseId(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "Cabecera";


            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Centro poblado";


            if (id.Equals("3"))
                //return "Cédula de Ciudadanía";
                return "Rural Disperso";

            return null;
        }

        private string getTipoViviendaNombre(string id)
        {
            if (id.Equals("1"))
                return "Casa";

            if (id.Equals("2"))
                return "Apartamento";

            if (id.Equals("3"))
                return "Cuarto";

            if (id.Equals("4"))
                return "Otro tipo de vivienda";

            if (id.Equals("5"))
                return "Vivienda indígena";

            return null;
        }

        private string getTipoMaterialParedesNombre(string id)
        {
            if (id.Equals("1"))
                return "Bloque, ladrillo, piedra, madera pulida";

            if (id.Equals("2"))
                return "Tapia pisada, adobe";

            if (id.Equals("3"))
                return "Bahareque";

            if (id.Equals("4"))
                return "Material prefabricado";

            if (id.Equals("5"))
                return "Madera burda, tabla, tablón";

            if (id.Equals("6"))
                return "Guadua, casa, esterilla, otro vegetal";

            if (id.Equals("7"))
                return "Zinc, tela, lona, cartón, latas, desechos, plástico";

            if (id.Equals("0"))
                return "Sin paredes";

            return null;
        }

        private string getTipoPisosNombre(string id)
        {
            if (id.Equals("1"))
                return "Alfombra o tapete, mármol, parque, madera pulida y lacada";

            if (id.Equals("2"))
                return "Baldosa, vinilo, tableta, ladrillo";

            if (id.Equals("3"))
                return "Cemento, gravilla";

            if (id.Equals("4"))
                return "Madera burda, madera en mal estado, tabla, tablón";

            if (id.Equals("5"))
                return "Tierra o arena";

            if (id.Equals("6"))
                return "otro";

            return null;
        }


        private string getEstadosSINO(string id)
        {
            if (id.Equals("1"))
                return "SI";

            if (id.Equals("2"))
                return "NO";

            return null;
        }

        private string getEstadosSINOFLUJO(string id)
        {
            if (id.Equals("1"))
                return "SI";

            if (id.Equals("2"))
                return "NO";

            if (id.Equals("9"))
                return "No";

            return null;
        }

        private string getSexo(string id)
        {
            if (id.Equals("1"))
                return "Hombre";


            if (id.Equals("2"))
                return "Mujer";

            return null;
        }

        private string getOcupacionVivienda(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "En arriendo o subarriendo";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Propia, la están pagando";

            if (id.Equals("3"))
                //return "Tarjeta de Identidad";
                return "Propia, totalmente pagada";

            if (id.Equals("4"))
                //return "Tarjeta de Identidad";
                return "Con permiso del propietario";

            if (id.Equals("5"))
                //return "Tarjeta de Identidad";
                return "Posesión sin título, ocupante de hecho";

            return null;
        }

        private string getTipoOrigenAgua(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "Acueducto";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Pozo con bomba";

            if (id.Equals("3"))
                //return "Tarjeta de Identidad";
                return "Pozo sin bomba, jagüey";

            if (id.Equals("4"))
                //return "Tarjeta de Identidad";
                return "Agua lluvia";

            if (id.Equals("5"))
                //return "Tarjeta de Identidad";
                return "Rio, quebrada, manantial o nacimiento";

            if (id.Equals("6"))
                //return "Tarjeta de Identidad";
                return "Pila publica";

            if (id.Equals("7"))
                //return "Tarjeta de Identidad";
                return "Carro tanque";

            if (id.Equals("8"))
                //return "Tarjeta de Identidad";
                return "Aguatero";

            if (id.Equals("9"))
                //return "Tarjeta de Identidad";
                return "Agua embotellada o en bolsa";

            return null;
        }

        private string getUsoCocina(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "De uso exclusivo de este hogar";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Compartido con hogares de la misma vivienda";

            if (id.Equals("3"))
                //return "Tarjeta de Identidad";
                return "Compartido con hogares de otras viviendas";

            if (id.Equals("4"))
                //return "Tarjeta de Identidad";
                return "No";

            return null;
        }

        private string getTipoEnergia(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "Electricidad";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Gas natural conectado a red pública";

            if (id.Equals("3"))
                //return "Registro Civil";
                return "Gas propano (en cilindro o pipeta)";

            if (id.Equals("4"))
                //return "Tarjeta de Identidad";
                return "Petróleo, gasolina, kerosene, alcohol, cocinol";

            if (id.Equals("5"))
                //return "Registro Civil";
                return "Carbón mineral";

            if (id.Equals("6"))
                //return "Tarjeta de Identidad";
                return "Material de desecho, leña, carbón de leña";

            if (id.Equals("7"))
                //return "Registro Civil";
                return "Ninguno (no cocina)";

            if (id.Equals("8"))
                //return "Tarjeta de Identidad";
                return "No";

            return null;
        }

        private string getTipoUsoAguaBeber(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "La usan tal como la obtienen";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "La hierven";

            if (id.Equals("3"))
                //return "Tarjeta de Identidad";
                return "Le echan cloro";

            if (id.Equals("4"))
                //return "Tarjeta de Identidad";
                return "Utilizan filtros";

            if (id.Equals("5"))
                //return "Tarjeta de Identidad";
                return "La decantan o usan filtros naturales";

            if (id.Equals("6"))
                //return "Tarjeta de Identidad";
                return "Compran agua embotellada o en bolsa";

            return null;
        }

        private string getPreparaAlimento(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "En un cuarto usado solo para cocinar";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "En un cuarto usado también para dormir";

            if (id.Equals("1"))
                //return "Registro Civil";
                return "En una sala - comedor";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "En un patio, corredor, enramada, al aire libre";

            if (id.Equals("1"))
                //return "Registro Civil";
                return "En ninguna parte (no preparan alimentos)";

            return null;
        }

        private string getEliminarBasura(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "La recogen los servicios del aseo";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "La entierran";

            if (id.Equals("3"))
                //return "Registro Civil";
                return "La queman";

            if (id.Equals("4"))
                //return "Tarjeta de Identidad";
                return "La tiran a un patio, lote, zanja o baldío";

            if (id.Equals("5"))
                //return "Registro Civil";
                return "La tiran a un rio, quebrada, caño o laguna";

            if (id.Equals("6"))
                //return "Tarjeta de Identidad";
                return "La recoge un servicio informal (zorra, carreta)";

            if (id.Equals("7"))
                //return "Tarjeta de Identidad";
                return "La eliminan de otra forma";

            return null;
        }

        private string getTipoSanitario(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "Con conexión a alcantarillado";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Con conexión a pozo séptico";

            if (id.Equals("3"))
                //return "Registro Civil";
                return "Sin conexión a alcantarillado ni a pozo séptico";

            if (id.Equals("4"))
                //return "Tarjeta de Identidad";
                return "Letrina, bajamar";

            if (id.Equals("5"))
                //return "Registro Civil";
                return "No tiene";

            return null;
        }

        private string getUbicacionSanitario(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "Dentro de la vivienda";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Fuera de la vivienda";

            if (id.Equals("9"))
                //return "Registro Civil";
                return "No";

            return null;
        }

        private string getUsoSanitario(string id)
        {
            if (id.Equals("1"))
                //return "Registro Civil";
                return "De uso exclusivo de este hogar";

            if (id.Equals("2"))
                //return "Tarjeta de Identidad";
                return "Compartido con hogares de la misma vivienda";

            if (id.Equals("3"))
                //return "Registro Civil";
                return "Compartido con hogares de otras viviendas";

            if (id.Equals("9"))
                //return "Registro Civil";
                return "No";

            return null;
        }

        private string tipoDocumento(string id)
        {
            if (id.Equals("1"))
                return "Registro civil";

            if (id.Equals("2"))
                return "Tarjeta de Identidad";

            if (id.Equals("3"))
                return "Cédula de ciudadanía";

            if (id.Equals("4"))
                return "Cédula de extranjería";

            if (id.Equals("5"))
                return "DNI (país de origen)";

            if (id.Equals("6"))
                return "Pasaporte";

            if (id.Equals("7"))
                return "Salvoconducto para refugiado";

            if (id.Equals("8"))
                return "Permiso Especial de Permanencia (PEP)";

            if (id.Equals("9"))
                return "Permiso de Protección Temporal (PPT)";

            return null;
        }

        private string getParentesco(string id)
        {
            if (id.Equals("1"))
                return "Jefe del hogar";

            if (id.Equals("2"))
                return "Cónyuge o compañero(a)";

            if (id.Equals("3"))
                return "Hijo(a), hijastro(a), hijo(a) adoptivo(a)";

            if (id.Equals("4"))
                return "Nieto(a)";

            if (id.Equals("5"))
                return "Padre, madre, padrastro, madrastra";

            if (id.Equals("6"))
                return "Hermano(a)";

            if (id.Equals("7"))
                return "Yerno / Nuera";

            if (id.Equals("8"))
                return "Abuelo(a)";

            if (id.Equals("9"))
                return "Suegro(a)";

            if (id.Equals("10"))
                return "Tío(a)";

            if (id.Equals("11"))
                return "Sobrino(a)";

            if (id.Equals("12"))
                return "Primo(a)";

            if (id.Equals("13"))
                return "Cuñado(a)";

            if (id.Equals("14"))
                return "Otro pariente";

            if (id.Equals("15"))
                return "Empleado(a) de servicio doméstico";

            if (id.Equals("16"))
                return "Pariente del servicio doméstico";

            if (id.Equals("17"))
                return "Pensionista";

            if (id.Equals("18"))
                return "Pariente de pensionista";

            if (id.Equals("19"))
                return "No pariente";

            return null;
        }

        private string getSeguridadSocial(string id)
        {
            if (id.Equals("1"))
                return "Contributivo";

            if (id.Equals("2"))
                return "Especial (Fuerzas Armadas, Ecopetrol, universidades públicas, magisterio)";

            if (id.Equals("3"))
                return "Subsidiado (EPS-S)";

            if (id.Equals("0"))
                return "Ninguna";

            if (id.Equals("9"))
                return "No sabe";

            return null;
        }

        private string getCuidadoNinos(string id)
        {
            if (id.Equals("1"))
                return "Asiste a un lugar comunitario, jardín o centro de desarrollo infantil o colegio";

            if (id.Equals("2"))
                return "Con su padre o madre en la casa";

            if (id.Equals("3"))
                return "Con su padre o madre en el trabajo";

            if (id.Equals("4"))
                return "Con empleada o niñera en la casa";

            if (id.Equals("5"))
                return "Al cuidado de un pariente de 18 años o más";

            if (id.Equals("6"))
                return "Al cuidado de un pariente menor de 18 años";

            if (id.Equals("7"))
                return "En casa solo";

            if (id.Equals("9"))
                return "No";

            return null;
        }

        private string getNivelEducativo(string id)
        {
            if (id.Equals("0"))
                return "Ninguno";

            if (id.Equals("1"))
                return "Preescolar";

            if (id.Equals("2"))
                return "Básica primaria (1o. - 5o)";

            if (id.Equals("3"))
                return "Básica secundaria (6o. - 9o.)";

            if (id.Equals("4"))
                return "Media (10o. 13o.)";

            if (id.Equals("5"))
                return "Técnico o tecnológico";

            if (id.Equals("6"))
                return "Universitario";

            if (id.Equals("7"))
                return "Postgrado";

            if (id.Equals("99"))
                return "No";

            return null;
        }

        private string getFondoPensiones(string id)
        {
            if (id.Equals("1"))
                return "SI";

            if (id.Equals("2"))
                return "NO";

            if (id.Equals("3"))
                return "Pensionado";

            if (id.Equals("9"))
                return "NO";

            return null;
        }

        private string getEmpleado(string id)
        {
            if (id.Equals("1"))
                return "Empleado de empresa particular";

            if (id.Equals("2"))
                return "Empleado del gobierno";

            if (id.Equals("3"))
                return "Empleado domestico";

            if (id.Equals("4"))
                return "Profesional independiente";

            if (id.Equals("5"))
                return "Trabajador independiente o por cuenta propia";

            if (id.Equals("6"))
                return "Patrón o empleador";

            if (id.Equals("7"))
                return "Trabajador de finca, tierra o parcela propia, en arriendo, aparceria o usufructo";

            if (id.Equals("8"))
                return "Trabajador sin remuneración";

            if (id.Equals("9"))
                return "Ayudante sin remuneración (hijo o familiar de: empleados domésticos, mayordomos, jornaleros, etc.)";

            if (id.Equals("10"))
                return "Jornalero o peón";

            if (id.Equals("99"))
                return "No";

            return null;
        }

        private string getActividadMes(string id)
        {
            if (id.Equals("1"))
                return "Trabajando";

            if (id.Equals("2"))
                return "Buscando trabajo";

            if (id.Equals("3"))
                return "Estudiando";

            if (id.Equals("4"))
                return "Oficios del Hogar";

            if (id.Equals("5"))
                return "Rentista";

            if (id.Equals("6"))
                return "Jubilado o pensionado";

            if (id.Equals("7"))
                return "Incapacitado permanentemente";

            if (id.Equals("0"))
                return "Sin actividad";

            if (id.Equals("9"))
                return "No";

            return null;
        }

        private string getSemenasTrabajo(string id)
        {
            if (id.Equals("999"))
                return "No";

            return id;
        }

        private string getEstadoCivil(string id)
        {
            if (id.Equals("1"))
                return "Unión libre";

            if (id.Equals("2"))
                return "Casado(a)";

            if (id.Equals("3"))
                return "Viudo(a)";

            if (id.Equals("4"))
                return "Separado(a) o divorciado(a)";

            if (id.Equals("5"))
                return "Soltero(a)";

            return null;
        }


        private string getPaisDocumento(string id)
        {
            if (id.Equals("00"))
                return "BOGOTÁ D.C.";

            if (id.Equals("862"))
                return "VENEZUELA";


            return null;
        }


        private string getDptoDocumento(string id)
        {
            if (id.Equals("00"))
                return "BOGOTÁ D.C.";



            return null;
        }

        private string getMcpioDocumento(string id)
        {
            if (id.Equals("00000"))
                return "BOGOTÁ D.C.";


            return null;
        }


    }
}

  