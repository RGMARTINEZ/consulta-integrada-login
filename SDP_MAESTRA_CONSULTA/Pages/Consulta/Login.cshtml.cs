﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using reCAPTCHA.AspNetCore.Attributes;
using SDP_MAESTRA_CONSULTA.Models;

namespace SDP_MAESTRA_CONSULTA.Pages.Consulta
{
    public class LoginModel : PageModel
    {
        private readonly SDP_MAESTRA_CONSULTA.Models.MAESTRA_APPContext _context;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IHttpClientFactory _clientFactory;

        [Display(Name = "Usuario requerido *")]
        [BindProperty(SupportsGet = true)]
        public string Username { get; set; }

        [Display(Name = "Clave requerido *")]
        [BindProperty(SupportsGet = true)]
        public string Password { get; set; }

        public Boolean MostrarValidacion { get; set; }

        public int entro = 0;
        public Usuario usuarioAutenticado = new Usuario();

        public LoginModel(SDP_MAESTRA_CONSULTA.Models.MAESTRA_APPContext context, ILogger<IndexModel> logger, IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _context = context;
            _configuration = configuration;
            _logger = logger;
            _clientFactory = clientFactory;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            String error = ValidarFormulario();

            if (error.Equals(""))
            {               
                Usuario usuario = _context.Usuario.Where(X => X.Username == Username && X.Password == Password).FirstOrDefault();

                if(usuario!= null)
                {
                    MostrarValidacion = false;
                    HttpContext.Session.SetString("UserName", Username);
                    return Redirect("~/Consulta/Index");
                }
                else
                {
                    MostrarValidacion = true;
                }
            }
            else
            {
                MostrarValidacion = true;
            }

            return Page();
        }


        private String ValidarFormulario()
        {
            String retorno = "";           

            if (Username == null || (Username != null && Username.Equals("")))
                retorno = retorno + "Username Requerido, ";

            if (Password == null || (Password != null && Password.Equals("")))
                retorno = retorno + "Password Requerido ";

            if(retorno!= null && !retorno.Equals(""))
            {
                retorno = retorno.Substring(0, retorno.Length - 2) +".";
            }

            return retorno;
        }

    }
}