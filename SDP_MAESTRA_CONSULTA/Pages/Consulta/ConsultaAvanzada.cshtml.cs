﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using reCAPTCHA.AspNetCore.Attributes;
using SDP_MAESTRA_CONSULTA.Models;

namespace SDP_MAESTRA_CONSULTA.Pages.Consulta
{
    public class ConsultaAvanzadaModel : PageModel
    {
        private readonly SDP_MAESTRA_CONSULTA.Models.MAESTRA_APPContext _context;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IHttpClientFactory _clientFactory;

        /// 
        /// Consulta Avanzada
        /// 
        [Display(Name = "Primer Nombre")]
        [BindProperty(SupportsGet = true)]
        public string PrimerNombre { get; set; }

        [Display(Name = "Primer Apellido")]
        [BindProperty(SupportsGet = true)]
        public string PrimerApellido { get; set; }

        [Display(Name = "Segundo Nombre")]
        [BindProperty(SupportsGet = true)]
        public string SegundoNombre { get; set; }

        [Display(Name = "Segundo Apellido")]
        [BindProperty(SupportsGet = true)]
        public string SegundoApellido { get; set; }

        [Display(Name = "Año de Nacimiento *")]
        [BindProperty(SupportsGet = true)]
        public string anio { get; set; }

        [Display(Name = "Mes de Nacimiento *")]
        [BindProperty(SupportsGet = true)]
        public string mes { get; set; }

        [Display(Name = "Día de Nacimiento *")]
        [BindProperty(SupportsGet = true)]
        public string dia { get; set; }

        public int entro = 0;
        public Maestra persona = new Maestra();
        public Boolean MostrarError { get; set; }
        public Boolean MostrarValidacion { get; set; }

        public ConsultaAvanzadaModel(SDP_MAESTRA_CONSULTA.Models.MAESTRA_APPContext context, ILogger<IndexModel> logger, IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _context = context;
            _configuration = configuration;
            _logger = logger;
            _clientFactory = clientFactory;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var primerNombre = string.IsNullOrWhiteSpace(PrimerNombre) ? null : PrimerNombre.ToUpper().Trim();
            var segundoNombre = string.IsNullOrWhiteSpace(SegundoNombre) ? null : SegundoNombre.ToUpper().Trim();
            var primerApellido = string.IsNullOrWhiteSpace(PrimerApellido) ? null : PrimerApellido.ToUpper().Trim();
            var segundoApellido = string.IsNullOrWhiteSpace(SegundoApellido) ? null : SegundoApellido.ToUpper().Trim();
            var anioCA = string.IsNullOrWhiteSpace(anio) ? null : anio.Trim();
            var mesCA = string.IsNullOrWhiteSpace(mes) ? null : mes.Trim();
            var diaCA = string.IsNullOrWhiteSpace(dia) ? null : dia.Trim();

            MostrarValidacion = false;
            if (ValidarFormulario(primerNombre, primerApellido, anioCA, mesCA, diaCA))
            {
                try
                {
                    string sql = "select * from consolidados_sisbeniv where Pri_nombre = '" + primerNombre + "' AND Pri_apellido = '" + primerApellido + "' ";

                    if (segundoNombre != null)
                        sql = sql + "and Seg_nombre = " + segundoNombre;

                    if (segundoApellido != null)
                        sql = sql + "and Seg_apellido = " + segundoApellido;

                    if (anioCA != null && mesCA != null && diaCA != null)
                        sql = sql + "and Fec_nacimiento = '" + diaCA + "/" + mesCA + "/" + anioCA.Substring(2) + "' ";



                    Maestra maestra  = _context.Maestra.FromSqlRaw(sql).FirstOrDefault();

                    if (maestra != null && maestra.Id > 0)
                    {
                        maestra.Tip_documento = tipoDocumentoStr(maestra.Tip_documento);
                        persona = maestra;
                        MostrarError = false;
                        MostrarValidacion = false;
                    }
                    else
                    {
                        MostrarValidacion = true;
                        MostrarError = false;
                        
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    MostrarError = true;
                    MostrarValidacion = false;
                }
            }
            else
            {
                if (entro == 0)
                {
                    entro++;
                }
                else
                {
                    MostrarValidacion = true;
                    MostrarError = false;
                }
            }

            return Page();
        }

        /// <summary>
        /// Metodo que valida el formulario
        /// </summary>
        /// <param name="primerNombre"></param>
        /// <param name="primerApellido"></param>
        /// <param name="anioCA"></param>
        /// <param name="mesCA"></param>
        /// <param name="diaCA"></param>
        /// <returns></returns>
        private Boolean ValidarFormulario(string primerNombre, string primerApellido, string anioCA, string mesCA, string diaCA)
        {
            if (string.IsNullOrEmpty(primerNombre) || string.IsNullOrEmpty(primerApellido) || string.IsNullOrEmpty(anioCA)
                || string.IsNullOrEmpty(mesCA) || string.IsNullOrEmpty(diaCA))
            {
                if (primerNombre!= null && primerApellido != null && anioCA!=null && mesCA != null && diaCA != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }                                 
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Metodo que transforma
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string tipoDocumentoStr(string id)
        {
            if (id.Equals("3"))
                return "Cédula de Ciudadanía";

            if (id.Equals("1"))
                return "Tarjeta de Identidad";

            if (id.Equals("3"))
                return "Documento de Ciudadano Extranjero";

            if (id.Equals("4"))
                return "Registro Civil";

            if (id.Equals("5"))
                return "Documento Nacional de Identidad";

            if (id.Equals("6"))
                return "Pasaporte";

            if (id.Equals("7"))
                return "Salvoconducto Migratorio";

            if (id.Equals("8"))
                return "Permiso Especial de Permanencia";

            return null;
        }
    }
}